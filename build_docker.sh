#!/bin/bash

# Por docker directo:
docker build --file Dockerfile.app-base . --no-cache -t vitalaire-base
docker build --file Dockerfile.webserver . --no-cache -t vitalaire-app
docker build --file Dockerfile.app-code . --no-cache -t vitalaire-webserver

docker network create vitalaire-net

# En producción, no creamos contenedor con base de datos
# Dos opciones:

# Con Docker Compose
docker-compose up -d airliquide-app airliquide-webserver

#Con docker directo
#docker run -d -p 9050:9000 --network=“vitalaire-net" --rm --name airliquide-app vitalaire-app
#docker run -d -p 80:80 --network=“vitalaire-net" --rm --name airliquide-webserver vitalaire-webserver