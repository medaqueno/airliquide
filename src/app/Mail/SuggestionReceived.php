<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

use App\Http\Modules\Suggestions\Models\Suggestion;
use App\Http\Modules\Users\Models\User;

class SuggestionReceived extends Mailable
{
    use Queueable, SerializesModels;

    protected $suggestion;

    protected $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Suggestion $suggestion, User $user)
    {
        $this->suggestion = $suggestion;
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this::to('medaqueno@gmail.com', 'Vitalaire')
            ->subject('Sugerencia VA Recibida')
            ->view('email.suggestionNotice', ['suggestion' => $this->suggestion, 'user' => $this->user]);
    }
}
