<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Session;

class SetLocale
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $locale = $request->header('lang') ? $request->header('lang') : Session::get('locale');

        if (in_array($locale, config('app.available_locales'))) {
            \App::setLocale($locale);
        }

        return $next($request);
    }
}
