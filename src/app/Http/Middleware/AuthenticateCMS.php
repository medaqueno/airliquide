<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Closure;
use App\Http\Modules\Auth\Exceptions\AuthException;

class AuthenticateCMS extends Middleware
{
    public function handle($request, Closure $next)
    {
        if (!empty(session('authenticated'))) {
            $request->session()->put('authenticated', time());
            return $next($request);
        }

        return redirect()->route('login')->with('status', __('auth.auth_error'));
    }
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    protected function redirectTo($request)
    {

         return redirect()->route('login')->with('status', __('auth.auth_error'));
    }
}
