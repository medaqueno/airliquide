<?php

namespace App\Http\Modules\Faqs;

use App\Http\Controllers\Controller;

use Illuminate\Http\Response;

use App\Http\Modules\Faqs\Models\Faq;

class GetByIdFaqsController extends Controller
{
    public function __invoke(int $id): Response
    {
        $faqs = Faq::find($id);

        if ($faqs === null) {
            abort(404);
        }

        return response($faqs);
    }
}
