<?php

namespace App\Http\Modules\Faqs;

use App\Http\Controllers\Controller;

use Illuminate\View\View;
use Illuminate\Http\RedirectResponse;

use App\Http\Modules\Faqs\Requests\EditFaqsRequest;
use App\Http\Modules\Faqs\Requests\CreateFaqsRequest;
use App\Http\Modules\Faqs\Models\Faq;

class AdminResourceFaqsController extends Controller
{
    public function index(): View
    {
        $items = Faq::paginate(20);

        return view('admin.faqs_index', ['items' => $items]);
    }

    public function show(int $id): View
    {
        $item = Faq::findorFail($id);

        return view('admin.faqs_detail', ['item' => $item]);
    }

    public function create(): View
    {
        return view('admin.faqs_create');
    }

    public function edit(int $id, EditFaqsRequest $request): RedirectResponse
    {
        $faqs = Faq::findOrFail($id);

        $faqs->fill($request->all());
        $faqs->save();

        return redirect()->back()->with('status', __('messages.save_sucess'));
    }

    public function store(CreateFaqsRequest $request): RedirectResponse
    {
        $faq = Faq::create($request->all());

        return redirect()->route('faqs_index');
    }

    public function delete(int $id): RedirectResponse
    {
        Faq::destroy($id);

        return redirect()->route('faqs_index');
    }
}
