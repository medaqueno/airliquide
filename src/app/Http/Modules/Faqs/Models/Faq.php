<?php

namespace App\Http\Modules\Faqs\Models;

use Illuminate\Database\Eloquent\Model;
use App\Http\Traits\HasTranslations;

class Faq extends Model
{
    use HasTranslations;

    public $translatable = [
        'title',
        'text'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'text',
        ];

    /**
     * The attributes that should be visible for arrays.
     *
     * @var array
     */
    protected $visible = [
        'id',
        'title',
        'text',
        ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
        'updated_at' => 'datetime:Y-m-d H:i:s'
        ];
}
