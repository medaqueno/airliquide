<?php

namespace App\Http\Modules\Faqs;

use App\Http\Controllers\Controller;

use Illuminate\Http\Response;

use App\Http\Modules\Faqs\Models\Faq;

class GetAllFaqsController extends Controller
{
    public function __invoke(): Response
    {
        $faqs = Faq::all();

        return response($faqs);
    }
}
