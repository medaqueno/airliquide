<?php

namespace App\Http\Modules\Utils;

use App\Http\Controllers\Controller;

class Helper extends Controller
{
    /**
     * Reset Password
     * @return string
     */
    public static function generatePassword(int $length = 3): string
    {
        return mb_strtolower(bin2hex(random_bytes($length)));
    }
}
