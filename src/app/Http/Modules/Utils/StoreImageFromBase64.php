<?php

namespace App\Http\Modules\Utils;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Storage;

class StoreImageFromBase64 extends Controller
{
    protected string $disk = 'public';

    public function __invoke(string $base64ImageData, string $routeImage, string $imageFileName): string
    {
        $base64ImageData = urldecode($base64ImageData);

        if (preg_match('/^data:image\/(\w+);base64,/', $base64ImageData, $type)) {
            $data = substr($base64ImageData, strpos($base64ImageData, ',') + 1);
            $type = strtolower($type[1]); // jpg, png, gif

            if (!in_array($type, [ 'jpg', 'jpeg', 'gif', 'png' ])) {
                throw new \Exception('invalid image type');
            }

            $data = base64_decode($data);

            if ($data === false) {
                throw new \Exception('base64_decode failed');
            }
        } else {
            throw new \Exception('did not match data URI with image data');
        }

        $imgRoute = $imageFileName . '.' . $type;

        if (env('STORE_UPLOADS') === true) {
            Storage::disk($this->disk)->put($routeImage . $imgRoute, $data);
        }

        return $imgRoute;
    }
}
