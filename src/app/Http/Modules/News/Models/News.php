<?php

namespace App\Http\Modules\News\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use App\Http\Traits\HasTranslations;

class News extends Model
{
    use HasTranslations;

    public $translatable = [
        'title',
        'intro',
        'text'
    ];

    protected $table = "news";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
     protected $fillable = [
        'title',
        'intro',
        'text',
        'img',
        'date',
        'active'
     ];

    /**
     * The attributes that should be visible for arrays.
     *
     * @var array
     */
     protected $visible = [
        'id',
        'title',
        'intro',
        'text',
        'img',
        'date',
        'active',
     ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
     protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
        'updated_at' => 'datetime:Y-m-d H:i:s',
        'date' => 'datetime:Y-m-d H:i:s',
        'active' => 'bool'
     ];

    /**
     * Adds route to image
     *
     * @param  string $value
     * @return string
     */

     public function getImgAttribute(?string $value): ?string
     {
         return empty($value) ? null : asset(Storage::disk('public')->url('news/' . $value));
     }
}
