<?php

namespace App\Http\Modules\News;

use App\Http\Controllers\Controller;

use Illuminate\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Str;

use Carbon\Carbon;
use App\Http\Modules\News\Requests\EditNewsRequest;
use App\Http\Modules\News\Requests\CreateNewsRequest;
use App\Http\Modules\News\Models\News;
use App\Http\Modules\Utils\StoreImageFromBase64;

class AdminResourceNewsController extends Controller
{
    protected string $storeImageRoute = 'news/';

    protected string $disk = 'public';

    public function index(): View
    {
        $items = News::paginate(20);

        return view('admin.news_index', ['items' => $items]);
    }

    public function show(int $id): View
    {
        $item = News::findorFail($id);

        return view('admin.news_detail', ['item' => $item]);
    }

    public function create(): View
    {
        return view('admin.news_create');
    }

    public function edit(int $id, EditNewsRequest $request): RedirectResponse
    {
        $news = News::findOrFail($id);

        if ($request->has('image')) {
            // Not an SPA, so, we are receiving multipart
            $file = $request->file('image');
            $name = 'news_' . uniqid() . Str::random(16) . "." . $file->extension();

            $imageUpload = $file->storeAs('news', $name, $this->disk);

            // Dont Mass assign.
            $request->merge(['img' => $name]);
        }

        if ($request->has('date')) {
            $date = $this->formatDateFromClient($request->input('date'));
            $request->merge(['date' => $date]);
        }

        $news->fill($request->all());
        $news->save();

        return redirect()->route('news_detail', ['id' => $id])->with('status', __('messages.save_sucess'));
    }

    public function store(CreateNewsRequest $request): RedirectResponse
    {
        if ($request->has('image')) {
            // Not an SPA, so, we are receiving multipart
            $file = $request->file('image');
            $name = 'news_' . uniqid() . Str::random(16) . "." . $file->extension();

            $imageUpload = $file->storeAs('news', $name, $this->disk);

            // Dont Mass assign.
            $request->merge(['img' => $name]);
        }

        if ($request->has('date')) {
            $date = $this->formatDateFromClient($request->input('date'));
            $request->merge(['date' => $date]);
        }

        $news = News::create($request->all());

        return redirect()->route('news_index');
    }

    public function delete(int $id): RedirectResponse
    {
        News::destroy($id);

        return redirect()->route('news_index');
    }


    /****/

    private function formatDateFromClient($date)
    {
        return Carbon::createFromFormat('d-m-Y H:i', $date)->format('Y-m-d H:i:s');
    }

    private function storeBase64Image(string $base64ImageData, string $prefixImageFileName): string
    {
        $routeImage = $this->storeImageRoute;
        $imageFileName = $prefixImageFileName . uniqid() . Str::random(16);
        $storeImage = new StoreImageFromBase64;

        return $storeImage($base64ImageData, $routeImage, $imageFileName);
    }
}
