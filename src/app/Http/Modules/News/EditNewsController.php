<?php

namespace App\Http\Modules\News;

use App\Http\Controllers\Controller;

use Illuminate\Http\Response;
use Illuminate\Support\Str;

use App\Http\Modules\News\Requests\EditNewsRequest;
use App\Http\Modules\News\Models\News;
use App\Http\Modules\Utils\StoreImageFromBase64;

class EditNewsController extends Controller
{
    protected string $storeImageRoute = 'news/';

    public function __invoke(int $id, EditNewsRequest $request): Response
    {
        $news = $this->findNews($id);

        if ($request->has('img')) {
             // Store image
            $image = $this->storeBase64Image($request->input('img'), 'news_');
            $request->merge(['img' => $image]);
        }

        $news->fill($request->all());
        $news->save();

        return response('', 204);
    }

    private function findNews($id)
    {
        $news = News::find($id);

        if ($news === null) {
            abort(404);
        }

        return $news;
    }

    private function storeBase64Image(string $base64ImageData, string $prefixImageFileName): string
    {
        $routeImage = $this->storeImageRoute;
        $imageFileName = $prefixImageFileName . uniqid() . Str::random(16);
        $storeImage = new StoreImageFromBase64;

        return $storeImage($base64ImageData, $routeImage, $imageFileName);
    }
}
