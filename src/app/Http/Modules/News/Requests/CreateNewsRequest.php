<?php

namespace App\Http\Modules\News\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateNewsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => ['required', 'max:190'],
            'intro' => ['string'],
            'text' => ['required'],
            'date' => ['required'],
            'image' => ['image', 'max:5120', 'mimes:jpeg,bmp,png']
        ];
    }
}
