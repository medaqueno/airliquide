<?php

namespace App\Http\Modules\News;

use App\Http\Controllers\Controller;

use Illuminate\Http\Response;
use Illuminate\Support\Str;

use App\Http\Modules\News\Requests\CreateNewsRequest;
use App\Http\Modules\News\Models\News;
use App\Http\Modules\Utils\StoreImageFromBase64;

class CreateNewsController extends Controller
{
    protected string $storeImageRoute = 'news/';

    public function __invoke(CreateNewsRequest $request): Response
    {
        if ($request->has('img')) {
             // Store image
            $image = $this->storeBase64Image($request->input('img'), 'news_');
            $request->merge(['img' => $image]);
        }

        $news = News::create($request->all());

        return response($news);
    }

    private function storeBase64Image(string $base64ImageData, string $prefixImageFileName): string
    {
        $routeImage = $this->storeImageRoute;
        $imageFileName = $prefixImageFileName . uniqid() . Str::random(16);
        $storeImage = new StoreImageFromBase64;

        return $storeImage($base64ImageData, $routeImage, $imageFileName);
    }
}
