<?php

namespace App\Http\Modules\News;

use App\Http\Controllers\Controller;

use Illuminate\Http\Response;

use App\Http\Modules\News\Models\News;

class GetByIdNewsController extends Controller
{
    public function __invoke(int $id): Response
    {
        $news = News::find($id);

        if ($news === null) {
            abort(404);
        }

        return response($news);
    }
}
