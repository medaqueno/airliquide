<?php

namespace App\Http\Modules\News;

use App\Http\Controllers\Controller;

use Illuminate\Http\Response;

use App\Http\Modules\News\Models\News;

class GetActiveNewsController extends Controller
{
    public function __invoke(): Response
    {
        $news = News::where('active', true)->get();

        return response($news);
    }
}
