<?php

namespace App\Http\Modules\Auth;

use App\Http\Controllers\Controller;

use Illuminate\Http\Response;

use App\Http\Modules\Utils\Helper;
use App\Http\Modules\Auth\Requests\CheckPhoneRequest;
use App\Http\Modules\Auth\Models\Confirmation;
use App\Http\Modules\Odigo\OdigoController;

class CheckPhoneController extends Controller
{
    public function __invoke(CheckPhoneRequest $request): Response
    {
        $createConfirmationData = $this->buildConfirmationData($request->all());

        $confirmation = $this->createConfirmation($createConfirmationData);

        // Send Password to the User
        $this->sendNotification($request, $createConfirmationData['code']);

        return response($confirmation);
    }

    private function buildConfirmationData(array $checkPhoneData): array
    {
        return [
            'token' => $checkPhoneData['phone_prefix'] . $checkPhoneData['phone'],
            'code' => Helper::generatePassword()
        ];
    }

    /**
     * Store confirmation data, that will be checked in RegisterController Action
     * @param  array  $createConfirmationData
     * @return Confirmation
     */
    private function createConfirmation(array $createConfirmationData): Confirmation
    {
        return Confirmation::create($createConfirmationData);
    }

    private function sendNotification(CheckPhoneRequest $request, string $code): bool
    {
        $sendNotification = new OdigoController;
        $phoneNumber = $request->input('phone_prefix') . $request->input('phone');

        $message = __('auth.sms_message', ['code' => $code]);

        return $sendNotification($phoneNumber, $message);
    }
}
