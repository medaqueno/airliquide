<?php

namespace App\Http\Modules\Auth;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Response;

use App\Http\Modules\Auth\Requests\LoginRequest;
use App\Http\Modules\Users\Models\User;
use App\Http\Modules\Auth\Exceptions\LoginException;

class LoginController extends Controller
{
    public function __invoke(LoginRequest $request): Response
    {
        $user = User::where('code_vitalaire', $request->input('code_vitalaire'))->first();

        if (! $user || ! Hash::check($request->input('password'), $user->password)) {
            throw new LoginException();
        }

        return response(['token' => $user->createToken($user->id)->plainTextToken]);
    }
}
