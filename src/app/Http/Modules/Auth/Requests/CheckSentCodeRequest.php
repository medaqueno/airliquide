<?php

namespace App\Http\Modules\Auth\Requests;

use App\BaseFormRequest;

use App\Rules\ExistsPhoneWithPrefixNew;

class CheckSentCodeRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phone_prefix' => ['required','string','max:4'],
            'phone' => ['required','digits:9','starts_with:6,7,9'],
            'code' => ['required'],
            'token' => ['required']
        ];
    }
}
