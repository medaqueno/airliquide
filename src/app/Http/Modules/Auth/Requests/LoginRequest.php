<?php

namespace App\Http\Modules\Auth\Requests;

use App\BaseFormRequest;

class LoginRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code_vitalaire' => ['required', 'string','max:190'],
            'password' => ['required','min:8','max:24'],
        ];
    }
}
