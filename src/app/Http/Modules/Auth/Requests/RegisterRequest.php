<?php

namespace App\Http\Modules\Auth\Requests;

use App\BaseFormRequest;

use App\Rules\NumSegSocial;
use App\Rules\ExistsPhoneWithPrefixNew;

class RegisterRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required','string','max:190'],
            'surname' => ['required','string','max:190'],
            'email' => ['email','filled','max:190'],
            'phone_prefix' => ['required','string','max:4'],
            'phone' => ['required','digits:9','starts_with:6,7,9', new ExistsPhoneWithPrefixNew($this->request->all())],
            'code_vitalaire' => ['required','string','max:190'],
            'num_ss' => ['required','digits:12', new NumSegSocial],
            'password' => ['required','min:8','max:24'],
            'token' => ['required'],
            'legal_notifications' =>['accepted'],
            'legal_terms' => ['accepted']
        ];
    }
}
