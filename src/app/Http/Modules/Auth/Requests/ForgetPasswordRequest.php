<?php

namespace App\Http\Modules\Auth\Requests;

use App\BaseFormRequest;

use App\Rules\NumSegSocial;

class ForgetPasswordRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code_vitalaire' => ['required','string','max:190'],
            'num_ss' => ['required','digits:12', new NumSegSocial],
        ];
    }
}
