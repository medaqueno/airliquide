<?php

namespace App\Http\Modules\Auth\Exceptions;

use Exception;

class ConfirmationException extends Exception
{
    public function render()
    {
        return response()->json([
            "errors" => [
                "error_code" => 'confirmation_sms_ko',
                "message" => __('errors.confirmation_sms_ko')
            ]
        ], 401);
    }
}
