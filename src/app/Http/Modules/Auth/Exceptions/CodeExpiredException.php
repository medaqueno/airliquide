<?php

namespace App\Http\Modules\Auth\Exceptions;

use Exception;

class CodeExpiredException extends Exception
{
    public function render()
    {
        return response()->json([
            "errors" => [
                "error_code" => 'confirmation_expired',
                "message" => __('errors.confirmation_expired')
            ]
        ], 401);
    }
}
