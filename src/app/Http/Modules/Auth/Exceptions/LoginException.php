<?php

namespace App\Http\Modules\Auth\Exceptions;

use Exception;

class LoginException extends Exception
{
    public function render()
    {
        return response()->json([
            "errors" => [
                "error_code" => 'auth_failed',
                "message" => __('auth.failed')
            ]
        ], 401);
    }
}
