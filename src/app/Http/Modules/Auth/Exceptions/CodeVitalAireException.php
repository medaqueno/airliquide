<?php

namespace App\Http\Modules\Auth\Exceptions;

use Exception;

class CodeVitalAireException extends Exception
{
    public function render()
    {
        return response()->json([
            "errors" => [
                "error_code" => 'code_vitalaire_not_found',
                "message" => __('errors.code_vitalaire_not_found')
            ]
        ], 404);
    }
}
