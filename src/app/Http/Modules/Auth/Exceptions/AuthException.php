<?php

namespace App\Http\Modules\Auth\Exceptions;

use Exception;

class AuthException extends Exception
{
    public function render()
    {
        return response()->json([
            "errors" => [
                "error_code" => 'auth_failed',
                "message" => __('auth.auth_error')
            ]
        ], 401);
    }
}
