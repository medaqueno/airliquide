<?php

namespace App\Http\Modules\Auth;

use App\Http\Controllers\Controller;

use Illuminate\Http\Response;

use App\Http\Modules\Utils\Helper;
use App\Http\Modules\Auth\Requests\ForgetPasswordRequest;
use App\Http\Modules\Odigo\OdigoController;
use App\Http\Modules\Users\Models\User;

class ForgetPasswordController extends Controller
{
    public function __invoke(ForgetPasswordRequest $request): Response
    {
        $newPassword = Helper::generatePassword(4);

        // Update Password in DB
        $user = $this->resetPassword($request, $newPassword);

        if ($user) {
            // Send Password to the User
            $this->sendNotification($user, $newPassword);
        }

        return response('');
    }

    private function resetPassword(ForgetPasswordRequest $request, string $password): ?User
    {
        $user = User::where('code_vitalaire', $request->input('code_vitalaire'))
            ->where('num_ss', $request->input('num_ss'))
            ->first();

        if ($user) {
            $user->password = $password;
            $user->save();
        }

        return $user;
    }

    private function sendNotification(User $user, string $newPassword): bool
    {
        $sendNotification = new OdigoController;
        $phoneNumber = $user->phone_prefix . $user->phone;

        $message = __('auth.change_password', ['changePassword' => $newPassword]);

        return $sendNotification($phoneNumber, $message);
    }
}
