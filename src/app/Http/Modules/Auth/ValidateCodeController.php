<?php

namespace App\Http\Modules\Auth;

use App\Http\Controllers\Controller;

use Illuminate\Http\Response;

use App\Http\Modules\Auth\Requests\ValidateCodeRequest;
use App\Http\Modules\Users\Models\User;
use App\Http\Modules\Auth\Exceptions\CodeVitalAireException;

class ValidateCodeController extends Controller
{
    public function __invoke(ValidateCodeRequest $request): Response
    {
        User::where('code_vitalaire', '=', $request->input('code_vitalaire'))->firstOr(function () {
            throw new CodeVitalAireException();
        });

        return response('');
    }
}
