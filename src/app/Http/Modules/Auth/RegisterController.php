<?php

namespace App\Http\Modules\Auth;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Response;

use App\Http\Modules\Auth\Exceptions\ConfirmationException;
use App\Http\Modules\Auth\Requests\RegisterRequest;
use App\Http\Modules\Auth\Models\Confirmation;
use App\Http\Modules\Users\Models\User;

class RegisterController extends Controller
{
    public function __invoke(RegisterRequest $request): Response
    {
        // Check Confirmation Data
        if (!$this->isConfirmationOK($request->all())) {
            throw new ConfirmationException();
        }

        // Delete Confirmation entry
        $this->deleteConfirmation($request->all());

        // Create new User
        $user = $this->createUser($request->all());

        // Auto Login User
        return response(['token' => $user->createToken($user->id)->plainTextToken]);
    }

    private function isConfirmationOK(array $registerData): bool
    {
        // Retrieve stored confirmation
        $confirmation = Confirmation::where('token', $registerData['token'])->first();
        if (!$confirmation) {
            throw new ConfirmationException();
        }

        // Check token received hash
        $checkToken = $registerData['phone_prefix'] . $registerData['phone'];

        $isTokenReceivedOK = Hash::check($checkToken, $registerData['token']);

        return $isTokenReceivedOK;
    }

    private function createUser(array $registerData): User
    {
        return User::updateOrCreate(
            [
                'code_vitalaire' => $registerData['code_vitalaire'],
                'num_ss' => $registerData['num_ss']
            ],
            $registerData
        );
    }

    private function deleteConfirmation(array $registerData): bool
    {
        return Confirmation::where('token', $registerData['token'])->delete();
    }
}
