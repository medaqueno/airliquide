<?php

namespace App\Http\Modules\Auth;

use App\Http\Controllers\Controller;

use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;

use App\Http\Modules\Utils\Helper;
use App\Http\Modules\Auth\Requests\CheckSentCodeRequest;
use App\Http\Modules\Auth\Models\Confirmation;
use App\Http\Modules\Auth\Exceptions\ConfirmationException;
use App\Http\Modules\Auth\Exceptions\CodeExpiredException;

class CheckSentCodeController extends Controller
{
    protected $expirationInterval = 5; // Code expiration time in minutes

    public function __invoke(CheckSentCodeRequest $request): Response
    {
        $isConfirmationOK = $this->isConfirmationOK($request->all());

        return response('');
    }

    private function isConfirmationOK(array $checkData): bool
    {
        // Retrieve stored confirmation
        $confirmation = Confirmation::where('token', $checkData['token'])->first();

        if (!$confirmation) {
            throw new ConfirmationException();
        }

        // Check code expiration
        $isCodeStillValid = $this->isCodeStillValid($confirmation);

        $isCodeReceivedOK = $this->isCodeReceivedOK($checkData, $confirmation);

        $isTokenReceivedOK = $this->isTokenReceivedOK($checkData);

        return true;
    }

    private function isCodeStillValid(Confirmation $confirmation): bool
    {
        $created = new Carbon($confirmation->created_at);

        if (Carbon::now()->diffInMinutes($created) > $this->expirationInterval) {
            Confirmation::where('token', $confirmation->token)->delete();

            throw new CodeExpiredException();
        }

        return true;
    }

    private function isCodeReceivedOK(array $checkData, Confirmation $confirmation): bool
    {
        $hashCode = Hash::make($checkData['code']);

        if (!Hash::check($checkData['code'], $confirmation['code'])) {
            throw new ConfirmationException();
        }

        return true;
    }

    private function isTokenReceivedOK(array $checkData): bool
    {
         // Check token received hash
        $checkToken = $checkData['phone_prefix'] . $checkData['phone'];

        if (!Hash::check($checkToken, $checkData['token'])) {
            throw new ConfirmationException();
        }

        return true;
    }
}
