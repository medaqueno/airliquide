<?php

namespace App\Http\Modules\Therapies\Models;

use Illuminate\Database\Eloquent\Model;

class TherapyUser extends Model
{
    protected $table = "therapies_users";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [

    ];

    /**
     * The attributes that should be visible for arrays.
     *
     * @var array
     */

    protected $visible = [
        'id',
        'therapies_id',
        'name',
        'description',
        'icon',
        'color',
        'data',
        'start_date',
        'end_date',
        'units',
        'posology_dosage',
        'posology_interval',
        'posology_duration',
        'doctor',
        'place',
        'active',
        'last_questions'
    ];


     /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
        'updated_at' => 'datetime:Y-m-d H:i:s',
        'start_date' => 'datetime:Y-m-d',
        'end_date' => 'datetime:Y-m-d'
    ];
}
