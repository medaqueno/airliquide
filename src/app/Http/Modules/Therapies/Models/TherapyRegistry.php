<?php

namespace App\Http\Modules\Therapies\Models;

use Illuminate\Database\Eloquent\Model;

class TherapyRegistry extends Model
{
    protected $table = "therapies_registry";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'therapies_id', 'start_time', 'end_time', 'duration'
    ];

    /**
     * The attributes that should be visible for arrays.
     *
     * @var array
     */

    protected $visible = [
        'therapies_id', 'start_time', 'end_time', 'duration', 'date'
    ];


     /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
        'updated_at' => 'datetime:Y-m-d H:i:s',
        'start_time' => 'datetime:Y-m-d H:i:s',
        'end_time' => 'datetime:Y-m-d H:i:s',
        'therapies_id' => 'integer',
    ];

     /**
     * Get therapies for the user.
     */
    public function therapies()
    {
        return $this->hasMany('App\Http\Modules\Therapies\Models\Therapy', 'id');
    }

    /**
     * Get the user that owns the registry.
     */
    public function user()
    {
        return $this->belongsTo('App\Http\Modules\Therapies\Models\User');
    }
}
