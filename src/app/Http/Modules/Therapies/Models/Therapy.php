<?php

namespace App\Http\Modules\Therapies\Models;

use Illuminate\Database\Eloquent\Model;

class Therapy extends Model
{
    protected $table = "therapies";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [

    ];

    /**
     * The attributes that should be visible for arrays.
     *
     * @var array
     */

    protected $visible = [
        'id',
        'name',
        'description',
        'icon',
        'color'
    ];

     /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
        'updated_at' => 'datetime:Y-m-d H:i:s'
    ];

    /**
     * Get therapies for the user.
     */
    public function therapies()
    {
        return $this->hasMany('App\Http\Modules\Therapies\Models\Therapy', 'id');
    }

     /**
     * Get the questions for the questionnaires.
     */
    public function questions()
    {
        return $this->hasMany('App\Http\Modules\Questionnaires\Models\Question', 'therapies_id');
    }
}
