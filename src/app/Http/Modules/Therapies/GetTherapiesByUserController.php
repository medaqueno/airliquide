<?php

namespace App\Http\Modules\Therapies;

use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Http\Modules\Therapies\Models\Therapy;
use App\Http\Modules\Therapies\Models\TherapyUser;

class GetTherapiesByUserController extends Controller
{
    public function __invoke(): Response
    {
        $user = auth()->user();

        $activeTherapies = $this->getGetActiveTherapiesByUser($user->id);

        // return response($activeTherapies);

        $lastQuestions = $this->getLastQuestionsAnswered($user->id);

        //return response($lastQuestions->where('therapies_id'));

        $response = $this->transformResponse($activeTherapies, $lastQuestions);


        return response($response);
    }

    private function getGetActiveTherapiesByUser(int $user_id): \Illuminate\Database\Eloquent\Collection
    {
        return TherapyUser::join('therapies', 'therapies_users.therapies_id', '=', 'therapies.id')
            ->where('therapies_users.user_id', '=', $user_id)
            ->where('therapies_users.active', '=', 1)
            ->get();
    }

    private function getLastQuestionsAnswered(int $user_id)
    {
         return DB::table('questionnaires_questions')
            ->selectRaw('MAX(questionnaires_users.created_at) as date, questionnaires.type, questionnaires_questions.therapies_id')
            ->join('questionnaires_users', 'questionnaires_questions.id', '=', 'questionnaires_users.questionnaires_questions_id')
            ->join('questionnaires', 'questionnaires_questions.questionnaires_id', '=', 'questionnaires.id')
            ->where('questionnaires_users.user_id', '=', $user_id)
            ->groupBy('questionnaires_questions.questionnaires_id')
            ->groupBy('questionnaires_questions.therapies_id')
            ->get();
    }

    private function transformResponse($activeTherapies, $lastQuestions)
    {
        foreach ($activeTherapies as $key => $item) {
            $lastQuestionsTherapy = $lastQuestions->where('therapies_id', $item->therapies_id);

            $prem = $lastQuestionsTherapy->firstWhere('type', 'prem');
            $prom = $lastQuestionsTherapy->firstWhere('type', 'prom');

            $activeTherapies[$key]['last_questions'] = [
                'prem' => $prem->date ?? null,
                'prom' => $prom->date ?? null
            ];
        }

        return $activeTherapies;
    }
}
