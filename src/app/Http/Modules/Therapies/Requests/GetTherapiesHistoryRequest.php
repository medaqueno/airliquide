<?php

namespace App\Http\Modules\Therapies\Requests;

use App\BaseFormRequest;

class GetTherapiesHistoryRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'start_time' => ['date_format:Y-m-d H:i:s'],
            'end_time' => ['date_format:Y-m-d H:i:s']
        ];
    }
}
