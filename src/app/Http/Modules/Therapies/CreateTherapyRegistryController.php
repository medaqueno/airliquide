<?php

namespace App\Http\Modules\Therapies;

use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Carbon\Carbon;

use App\Http\Modules\Therapies\Models\Therapy;
use App\Http\Modules\Therapies\Models\TherapyUser;
use App\Http\Modules\Therapies\Models\TherapyRegistry;
use App\Http\Modules\Therapies\Requests\CreateTherapyRegistryRequest;

class CreateTherapyRegistryController extends Controller
{
    public function __invoke(int $therapies_id, CreateTherapyRegistryRequest $request): Response
    {
        $user = auth()->user();

        return response($this->createTherapyRegistry($user->id, $request));
    }

    private function createTherapyRegistry(int $user_id, CreateTherapyRegistryRequest $request): TherapyRegistry
    {
        return TherapyRegistry::create([
            'user_id' => $user_id,
            'therapies_id' => $request->input('therapies_id'),
            'start_time' => $request->input('start_time'),
            'end_time' => $request->input('end_time'),
            'duration' => $this->calcDuration($request->input('start_time'), $request->input('end_time')),
        ]);
    }

    private function calcDuration(string $start_time, string $end_time): int
    {
        $start_time = new Carbon($start_time);
        $end_time = new Carbon($end_time);

        return $start_time->diffInSeconds($end_time);
    }
}
