<?php

namespace App\Http\Modules\Therapies;

use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Support\Facades\DB;

use App\Http\Modules\Therapies\Models\TherapyRegistry;
use App\Http\Modules\Therapies\Models\TherapyUser;
use App\Http\Modules\Therapies\Requests\GetTherapiesHistoryRequest;

class GetTherapiesHistoryController extends Controller
{
    public function __invoke($therapy_id, GetTherapiesHistoryRequest $request): Response
    {
        $user = auth()->user();

        // Get User Therapies
        $therapies = $this->getAllTherapiesByUserId($user->id, $therapy_id);

        $therapies_registries = [];

        // Create days and empty days for each therapy
        foreach ($therapies as $therapy) {
            $therapy_empty_calendar = $this->getPeriod($request, $therapy);

            $therapy_history_registries = $this->getTherapiesPlainHistory($user->id, $therapy, $request);

            $merged = $this->mergeRegistryAndTherapyCalendar($therapy_empty_calendar, $therapy_history_registries, $therapy['therapies_id']);

            unset($therapy_empty_calendar, $therapy_history_registries);

            $therapies_registries[] = $merged;
        }

        // Merge different therapies calendars into one and group them by date (daily)
        $therapies_registries_merged = array_merge(...$therapies_registries);
        unset($therapies_registries);
        $therapies_registries_merged = collect($therapies_registries_merged)->groupBy('date')->toArray();

        $final_response = $this->buildFinalResponse($therapies_registries_merged);

        return response($final_response);
    }

    private function buildFinalResponse($registries)
    {
        $response = [];
        foreach ($registries as $key => $item) {
            $response[] = [
                "date" => $key,
                "therapies" => $item
            ];
        }

        return $response;
    }

    private function getTherapiesPlainHistory(int $user_id, array $therapy, GetTherapiesHistoryRequest $request): array
    {
        $queryResult = TherapyRegistry::select(DB::raw('therapies_id, start_time, end_time, duration, DATE_FORMAT(start_time, "%Y-%m-%d") as date'))
        ->where('therapies_registry.user_id', '=', $user_id)
        ->where('therapies_registry.therapies_id', '=', $therapy['therapies_id']);

        if ($request->has('start_date') && $request->has('end_date')) {
            $start_date = Carbon::parse($request->input('start_date'))->startOfDay();
            $end_date = Carbon::parse($request->input('end_date'))->endOfDay();

            $queryResult->where('start_time', '>=', $start_date)
               ->where('end_time', '<=', $end_date);
        }

        return $queryResult->get()->toArray();
    }

    private function mergeRegistryAndTherapyCalendar($therapy_empty_calendar, $therapy_history_registries, int $therapy_id)
    {
        // Replaces dates with registries
        foreach ($therapy_history_registries as $key => $item) {
            $empty_slot = array_search($item['date'], $therapy_empty_calendar);

            if ($empty_slot !== false) {
                $therapy_empty_calendar[$empty_slot] = $item;
            } else {
                $therapy_empty_calendar[] = $item;
            }
        }

        // Fill with empty objects where proceeds
        foreach ($therapy_empty_calendar as $key => $item) {
            if (is_string($item)) {
                $therapy_empty_calendar[$key] = [
                    "therapies_id" => $therapy_id,
                    "start_time" => "",
                    "end_time" => "",
                    "duration" => 0,
                    "date" => $item
                ];
            }
        }

        return $therapy_empty_calendar;
    }

    private function getAllTherapiesByUserId(int $user_id, $therapy_id): array
    {
        $query = TherapyUser::select(['therapies_id', 'posology_interval', 'start_date', 'end_date'])->where('user_id', '=', $user_id);

        if ($therapy_id !== 'all') {
            $query->where('therapies_id', '=', $therapy_id);
        }

        return $query->get()->toArray();
    }

    /**
     * Interval MUST BE PASSED IN MINUTES
     * @param  GetTherapiesHistoryRequest $request  since, to dates
     * @param  int                        $interval MINUTES
     * @return array
     */
    private function getPeriod(GetTherapiesHistoryRequest $request, array $therapy): array
    {

        $start_date = Carbon::createFromFormat('Y-m-d', $therapy['start_date']);
        $end_date = Carbon::createFromFormat('Y-m-d', $therapy['end_date']);

        $limit_since = $request->input('start_date') ?? '2000-01-01';
        $limit_to = $request->input('end_date') ?? '2100-01-01';

        $since = Carbon::createFromFormat('Y-m-d', $limit_since);
        $to = Carbon::createFromFormat('Y-m-d', $limit_to);

        // Constrain intervals to Therapy dates
        if ($start_date->greaterThan($since)) {
            $since = $start_date;
        }

        if ($end_date->lessThan($to)) {
            $to = $end_date;
        }

        $period = CarbonPeriod::create($since, $therapy['posology_interval'] . ' minutes', $to);

        $dates = [];
        foreach ($period as $date) {
            $dates[] = $date->format('Y-m-d');
        }

        return $dates;
    }
}
