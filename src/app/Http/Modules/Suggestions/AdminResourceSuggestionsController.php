<?php

namespace App\Http\Modules\Suggestions;

use App\Http\Controllers\Controller;

use Illuminate\View\View;

use App\Http\Modules\Suggestions\Models\Suggestion;

class AdminResourceSuggestionsController extends Controller
{
    public function index(): View
    {
        $suggestions = Suggestion::paginate(20);

        return view('admin.suggestions_index', ['suggestions' => $suggestions]);
    }

    public function show(int $id): View
    {
        $suggestion = Suggestion::find($id);

        return view('admin.suggestions_detail', ['suggestion' => $suggestion]);
    }
}
