<?php

namespace App\Http\Modules\Suggestions;

use App\Http\Controllers\Controller;

use Illuminate\Http\Response;
use Illuminate\Support\Facades\Mail;

use App\Http\Modules\Suggestions\Requests\CreateSuggestionRequest;
use App\Http\Modules\Suggestions\Models\Suggestion;
use App\Http\Modules\Users\Models\User;
use App\Mail\SuggestionReceived;

class CreateSuggestionController extends Controller
{
    public function __invoke(CreateSuggestionRequest $request): Response
    {
        $suggestion = new Suggestion($request->all());

        $user = \Auth::user();
        $user->suggestions()->save($suggestion);

        $this->SuggestionReceived($suggestion, $user);

        return response('', 204);
    }

    private function SuggestionReceived(Suggestion $suggestion, User $user): void
    {
        Mail::send(new SuggestionReceived($suggestion, $user));
    }
}
