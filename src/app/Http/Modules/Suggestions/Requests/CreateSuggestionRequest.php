<?php

namespace App\Http\Modules\Suggestions\Requests;

use App\BaseFormRequest;

class CreateSuggestionRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'text' => ['required','string','max:500']
        ];
    }
}
