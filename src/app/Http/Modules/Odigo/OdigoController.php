<?php

namespace App\Http\Modules\Odigo;

use App\Http\Controllers\Controller;

class OdigoController extends Controller
{
    public function __invoke(string $phone, string $message)
    {
        $this->requestOdigo($phone, $message);

        return true;
    }

    private function requestOdigo(string $phone, string $message): void
    {
        $xml_post_string = $this->buildRequest($phone, $message);

        // \Log::info($xml_post_string);

        $headers = [
                        "Content-type: text/xml;charset=\"utf-8\"",
                        "Accept: text/xml",
                        "Cache-Control: no-cache",
                        "Pragma: no-cache",
                        "Content-length: ".strlen($xml_post_string),
                    ];

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_URL, env('ODIGO_HOST'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($ch);

        curl_close($ch);
        // \Log::info($response);
    }

    private function buildRequest(string $phone, string $message): string
    {
        return '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:sch="http://www.prosodie.com/pcm/pushMessage/schemas">
                    <soapenv:Header/>
                    <soapenv:Body>
                      <sch:PushShortMessageRequest>
                         <sch:Client id = "' . env('ODIGO_USER') . '" password = "' . env('ODIGO_PASSWORD') . '"/>
                         <sch:SMSRecipients>
                            <sch:SMSRecipient address="' . $phone . '"/>
                         </sch:SMSRecipients>
                         <sch:ShortMessage origin="E216903031" replyTo="E216903031" addressType="MSISDN" priority="STANDARD">' . $message . '</sch:ShortMessage>
                      </sch:PushShortMessageRequest>
                    </soapenv:Body>
                </soapenv:Envelope>';
    }
}
