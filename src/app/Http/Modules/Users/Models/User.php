<?php

namespace App\Http\Modules\Users\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Support\Facades\Hash;

class User extends Authenticatable
{
    use HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'surname',
        'num_ss',
        'code_vitalaire',
        'lang',
        'profile_img',
        'phone_prefix',
        'phone',
        'legal_terms',
        'legal_notifications',
        'num_ss',
        'email',
        'password',
        'active'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'created_at', 'updated_at', 'deleted_at'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
        'updated_at' => 'datetime:Y-m-d H:i:s',
        'deleted_at' => 'datetime:Y-m-d H:i:s'
    ];

    /**
     * Set Default attribute values
     *
     * @var array
     */
     protected $attributes = [
        'lang' => 'es',
        'phone_prefix' => '+34',
        'active' => 1
     ];

    /**
     * Hash password by default
     *
     * @param  string  $value
     * @return void
     */
     public function setPasswordAttribute($value)
     {
         $this->attributes['password'] = Hash::make($value);
     }

     /**
     * Get the suggestions for the user.
     */
     public function suggestions()
     {
         return $this->hasMany('App\Http\Modules\Suggestions\Models\Suggestion');
     }

     /**
     * Get the suggestions for the user.
     */
     public function therapyRegistries()
     {
         return $this->hasMany('App\Http\Modules\Therapies\Models\TherapyRegistry');
     }
}
