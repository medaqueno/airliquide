<?php

namespace App\Http\Modules\Users;

use App\Http\Controllers\Controller;

use Illuminate\Http\Response;

use App\Http\Modules\Users\Requests\ChangePasswordRequest;

class ChangePasswordController extends Controller
{
    public function __invoke(ChangePasswordRequest $request): Response
    {
        $user = \Auth::user();

        $user->password = $request->input('password');
        $user->save();

        return response('', 204);
    }
}
