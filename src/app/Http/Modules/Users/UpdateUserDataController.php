<?php

namespace App\Http\Modules\Users;

use App\Http\Controllers\Controller;

use Illuminate\Http\Response;
use Illuminate\Support\Str;

use App\Http\Modules\Utils\StoreImageFromBase64;
use App\Http\Modules\Users\Requests\UpdateUserDataRequest;

class UpdateUserDataController extends Controller
{
    protected string $storeImageRoute = 'public/profile/';

    public function __invoke(UpdateUserDataRequest $request): Response
    {
        if ($request->has('profile_img')) {
             // Store image
            $profile_image = $this->storeBase64Image($request->input('profile_img'), 'profile_');
            $request->merge(['profile_img' => $profile_image]);
        }

        $user = \Auth::user();

        $user->fill($request->all());
        $user->save();

        return response('', 204);
    }

    private function storeBase64Image(string $base64ImageData, string $prefixImageFileName): string
    {
        $routeImage = $this->storeImageRoute;
        $imageFileName = $prefixImageFileName . uniqid() . Str::random(16);
        $storeImage = new StoreImageFromBase64;

        return $storeImage($base64ImageData, $routeImage, $imageFileName);
    }
}
