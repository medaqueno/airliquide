<?php

namespace App\Http\Modules\Users;

use App\Http\Controllers\Controller;

use Illuminate\Http\Response;

use App\Http\Modules\Users\Models\User;
use App\Http\Modules\Questionnaires\GetQuestionsController;

class GetUserDataController extends Controller
{
    public function __invoke(): Response
    {
        $user = \Auth::user();

        return response($user);
    }
}
