<?php

namespace App\Http\Modules\Users\Requests;

use App\BaseFormRequest;

use App\Rules\NumSegSocial;
use App\Rules\ExistsPhoneWithPrefixUpdating;

class UpdateUserDataRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['string','max:190'],
            'surname' => ['string','max:190'],
            'email' => ['email','filled','unique:users,email,' . \Auth::user()->id . '','max:190'],
            'phone_prefix' => ['string','max:4'],
            'phone' => ['digits:9','starts_with:6,7,9', new ExistsPhoneWithPrefixUpdating($this->request->all(), \Auth::user()->id)],
            'code_vitalaire' => ['unique:users,code_vitalaire,' . \Auth::user()->id . '','string','max:190'],
            'num_ss' => ['unique:users,num_ss,' . \Auth::user()->id . '','digits:12', new NumSegSocial],
            'profile_img' => ['filled']
        ];
    }
}
