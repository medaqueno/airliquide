<?php

namespace App\Http\Modules\Questionnaires;

use App\Http\Controllers\Controller;

use Illuminate\Http\Response;
use Illuminate\Support\Arr;

use App\Http\Modules\Questionnaires\Requests\AnswerQuestionsRequest;
use App\Http\Modules\Questionnaires\Models\UserAnswer;
use App\Http\Modules\Questionnaires\Models\Question;
use Carbon\Carbon;

class AnswerQuestionsController extends Controller
{

    public function __invoke(int $questionnaires_id, AnswerQuestionsRequest $request): Response
    {
        $user = auth()->user();

        $answers = Arr::except($request->all(), 'questionnaires_id');

        // Contar el total de preguntas del cuestionario
        $totalQuestions = $this->totalQuestions($questionnaires_id);

        // Ver cuál es el valor de ronda parcial actual
        $maxPartialRound = $this->getCurrentPartialRound($user->id, $questionnaires_id);

        // Ver cuál es el valor actual de ronda completa actual
        $maxCompleteRound = $this->getCurrentCompleteRound($user->id, $questionnaires_id);

        // Preguntas contestadas para la ronda completa actual del usuario
        $questionsAnsweredInRound = $this->getQuestionsAnsweredInRound($user->id, $questionnaires_id, $maxCompleteRound);

        // si el total de preguntas contestadas es igual al total preguntas cuestionario, guardamos el valor de complete round, actual+1
        if ($totalQuestions === $questionsAnsweredInRound->count()) {
            ++$maxCompleteRound;
        }

        ++$maxPartialRound;

        $this->storeAnswers($answers, $maxPartialRound, $maxCompleteRound, $user->id);

        return response('', 201);
    }

    private function totalQuestions(int $questionnaires_id): int
    {
        return Question::where('questionnaires_id', '=', $questionnaires_id)
            ->count();
    }

    private function getCurrentPartialRound(int $user_id, int $questionnaires_id): int
    {
        $response = UserAnswer::where('questionnaires_users.user_id', '=', $user_id)
            ->join('questionnaires_questions', 'questionnaires_users.questionnaires_questions_id', '=', 'questionnaires_questions.id')
            ->where('questionnaires_id', '=', $questionnaires_id)
            ->max('partial_round');

        return $response ??= 0; // If null, it's first round
    }

    private function getCurrentCompleteRound(int $user_id, int $questionnaires_id): int
    {
        $response = UserAnswer::where('questionnaires_users.user_id', '=', $user_id)
            ->join('questionnaires_questions', 'questionnaires_users.questionnaires_questions_id', '=', 'questionnaires_questions.id')
            ->where('questionnaires_id', '=', $questionnaires_id)
            ->max('complete_round');

        return $response ??= 1; // If null, it's first round
    }

    private function getQuestionsAnsweredInRound(int $user_id, int $questionnaires_id, int $maxCompleteRound): \Illuminate\Database\Eloquent\Collection
    {
           $questionsAnsweredInRound = UserAnswer::join('questionnaires_questions', 'questionnaires_users.questionnaires_questions_id', '=', 'questionnaires_questions.id')
            ->where('questionnaires_questions.questionnaires_id', '=', $questionnaires_id)
            ->where('questionnaires_users.user_id', '=', $user_id)
            ->where('questionnaires_users.complete_round', '=', $maxCompleteRound)
            ->get();

        return $questionsAnsweredInRound;
    }

    private function storeAnswers(array $answers, int $maxPartialRound, int $maxCompleteRound, int $user_id): bool
    {
        $now = Carbon::now();

        $answersArray = [];
        foreach ($answers as &$item) {
            $answersArray[] = [
                'user_id' => $user_id,
                'partial_round' => $maxPartialRound,
                'complete_round' => $maxCompleteRound,
                'questionnaires_questions_id' => $item['question_id'],
                'questionnaires_answers_id' => $item['answer_id'],
                'created_at' => $now,
                'updated_at' => $now
             ];
        }

        return UserAnswer::insert($answersArray);
    }
}
