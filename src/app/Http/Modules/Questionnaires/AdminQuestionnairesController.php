<?php

namespace App\Http\Modules\Questionnaires;

use App\Http\Controllers\Controller;

use Illuminate\View\View;
use Illuminate\Http\RedirectResponse;

use App\Http\Modules\Questionnaires\Requests\CreateQuestionsRequest;
use App\Http\Modules\Questionnaires\Requests\CreateQuestionnaireRequest;
use App\Http\Modules\Questionnaires\Models\Question;
use App\Http\Modules\Therapies\Models\Therapy;

class AdminQuestionnairesController extends Controller
{
    public function index(): View
    {
        $items = Question::select([
                'questionnaires_questions.id',
                'questionnaires_questions.text',
                'therapies.name as therapy_name',
                'therapies.id as therapies_id',
                'questionnaires.name as questionnaires_name',
                'questionnaires.id as questionnaires_id'
            ])
            ->join('therapies', 'questionnaires_questions.therapies_id', '=', 'therapies.id')
            ->join('questionnaires', 'questionnaires_questions.questionnaires_id', '=', 'questionnaires.id')
            ->paginate(20);

        return view('admin.questions_index', ['items' => $items]);
    }

    public function show(int $id): View
    {
        $item = Question::findorFail($id);

        $therapies = $this->getTherapies();

        return view('admin.questions_detail', ['item' => $item, 'therapies' => $therapies]);
    }

    public function create(): View
    {
        $therapies = $this->getTherapies();

        return view('admin.questions_create', ['therapies' => $therapies]);
    }

    public function edit(int $id, CreateQuestionsRequest $request): RedirectResponse
    {
        $questions = Question::findOrFail($id);

        $questions->fill($request->all());
        $questions->save();

        return redirect()->route('questions_detail', ['id' => $id])->with('status', __('messages.save_sucess'));
    }

    public function store(CreateQuestionsRequest $request): RedirectResponse
    {
        $question = Question::create($request->all());

        return redirect()->route('questions_index');
    }

    public function delete(int $id): RedirectResponse
    {
        Question::destroy($id);

        return redirect()->route('questions_index');
    }

    private function getTherapies()
    {
        return Therapy::orderBy('name')->get();
    }
}
