<?php

namespace App\Http\Modules\Questionnaires;

use App\Http\Controllers\Controller;

use Illuminate\View\View;
use Illuminate\Http\RedirectResponse;

use App\Http\Modules\Questionnaires\Requests\AdminQuestionAnswerRequest;
use App\Http\Modules\Questionnaires\Models\Question;
use App\Http\Modules\Questionnaires\Models\QuestionAnswer;

class AdminAnswersController extends Controller
{
    public function index(int $question_id): View
    {
        $question = Question::find($question_id);

        $items = QuestionAnswer::where('questionnaires_questions_id', '=', $question_id)->paginate(20);

        return view('admin.answers_index', ['question_id' => $question_id, 'items' => $items, 'question' => $question]);
    }

    public function show(int $question_id, int $answer_id): View
    {
        $item = QuestionAnswer::where('questionnaires_questions_id', '=', $question_id)->findorFail($answer_id);

        return view('admin.answers_detail', ['question_id' => $question_id, 'answer_id' => $answer_id, 'item' => $item]);
    }

    public function create(int $question_id): View
    {
        return view('admin.answers_create', ['question_id' => $question_id]);
    }

    public function edit(int $question_id, int $answer_id, AdminQuestionAnswerRequest $request): RedirectResponse
    {
        $questions = QuestionAnswer::where('questionnaires_questions_id', '=', $question_id)->findOrFail($answer_id);

        $questions->fill($request->all());
        $questions->save();

        return redirect()->route('answers_detail', ['question_id' => $question_id, 'answer_id' => $answer_id])->with('status', __('messages.save_sucess'));
    }

    public function store(int $question_id, AdminQuestionAnswerRequest $request): RedirectResponse
    {
        $question = QuestionAnswer::create($request->all());

        return redirect()->route('answers_index', ['question_id' => $question_id]);
    }

    public function delete(int $question_id, int $answer_id): RedirectResponse
    {
        QuestionAnswer::destroy($answer_id);

        return redirect()->route('answers_index', ['question_id' => $question_id]);
    }
}
