<?php

namespace App\Http\Modules\Questionnaires\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AdminQuestionAnswerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'text' => ['required', 'max:100'],
            'value' => ['required', 'max:20']
        ];
    }
}
