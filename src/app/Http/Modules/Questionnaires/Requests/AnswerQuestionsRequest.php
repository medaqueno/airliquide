<?php

namespace App\Http\Modules\Questionnaires\Requests;

use App\BaseFormRequest;

class AnswerQuestionsRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
         // 'question_id' => ['required'],
         // 'answer_id' => ['required']
        ];
    }
}
