<?php

namespace App\Http\Modules\Questionnaires;

use App\Http\Controllers\Controller;

use Illuminate\Http\Response;
use \Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Http\Modules\Questionnaires\Models\Question;

class GetQuestionsController extends Controller
{
    public function __invoke(int $questionnaires_id, int $therapies_id, Request $request): Response
    {
        $user = auth()->user();

        $maxCompleteRound = $this->getMaxCompleteRoundByQuestionnaireId($user->id, $questionnaires_id, $therapies_id);
        $totalQuestions = $this->getTotalQuestionsInQuestionnaire($questionnaires_id, $therapies_id);
        $questionsAnsweredInRound = $this->getQuestionsAnsweredInRound($user->id, $questionnaires_id, $maxCompleteRound, $therapies_id);

        $questions = Question::select(['id', 'text'])
            ->with('answers')
            ->where('questionnaires_questions.questionnaires_id', '=', $questionnaires_id)
            ->where('questionnaires_questions.therapies_id', '=', $therapies_id);

        if (count($questionsAnsweredInRound) < $totalQuestions) {
            // Retrieve pending questions which have not been answered yet this round
             $questions->whereNotIn('questionnaires_questions.id', $questionsAnsweredInRound);
        }

        $limit = $request->has('limit') ? $request->input('limit') : env('LIMIT_QUESTIONS');

        return response($questions->inRandomOrder()->limit($limit)->get());
    }

    private function getMaxCompleteRoundByQuestionnaireId(int $user_id, int $questionnaires_id, int $therapies_id): int
    {
         $maxCompleteRound = DB::table('questionnaires_questions')
            ->where('questionnaires_questions.questionnaires_id', '=', $questionnaires_id)
            ->where('questionnaires_questions.therapies_id', '=', $therapies_id)
            ->join('questionnaires_users', 'questionnaires_questions.id', '=', 'questionnaires_users.questionnaires_questions_id')
            ->where('questionnaires_users.user_id', '=', $user_id)
            ->max('questionnaires_users.complete_round');

        return $maxCompleteRound ??= 1; // If null, it's first round
    }

    private function getTotalQuestionsInQuestionnaire(int $questionnaires_id, int $therapies_id): int
    {
         return DB::table('questionnaires_questions')
            ->where('questionnaires_questions.questionnaires_id', '=', $questionnaires_id)
            ->where('questionnaires_questions.therapies_id', '=', $therapies_id)
            ->count();
    }

    private function getQuestionsAnsweredInRound(int $user_id, int $questionnaires_id, int $maxCompleteRound, int $therapies_id): array
    {
         $questionsAnsweredInRound = DB::table('questionnaires_users')
            ->select(['questionnaires_users.questionnaires_questions_id'])
            ->join('questionnaires_questions', 'questionnaires_users.questionnaires_questions_id', '=', 'questionnaires_questions.id')
            ->where('questionnaires_questions.questionnaires_id', '=', $questionnaires_id)
            ->where('questionnaires_users.user_id', '=', $user_id)
            ->where('questionnaires_users.complete_round', '=', $maxCompleteRound)
            ->where('questionnaires_questions.therapies_id', '=', $therapies_id)
            ->get();

        return $questionsAnsweredInRound->pluck('questionnaires_questions_id')->all();
    }
}
