<?php

namespace App\Http\Modules\Questionnaires;

use App\Http\Controllers\Controller;

use Illuminate\Http\Response;

use App\Http\Modules\Questionnaires\Requests\CreateQuestionnaireRequest;
use App\Http\Modules\Questionnaires\Models\Questionnaire;

class AdminCreateQuestionnairesController extends Controller
{
    public function __invoke(CreateQuestionnaireRequest $request): Response
    {
        $questionnaire = Questionnaire::create($request->all());

        return response($questionnaire);
    }
}
