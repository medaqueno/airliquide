<?php

namespace App\Http\Modules\Questionnaires\Models;

use Illuminate\Database\Eloquent\Model;
use App\Http\Traits\HasTranslations;
use Illuminate\Support\Str;

class QuestionAnswer extends Model
{
    use HasTranslations;

    public $translatable = [
        'text'
    ];

    protected $table = "questionnaires_answers";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
    ];

    /**
     * The attributes that should be visible for arrays.
     *
     * @var array
     */
    protected $visible = [
        'id', 'text', 'value'
    ];

     /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
        'updated_at' => 'datetime:Y-m-d H:i:s'
    ];

    /**
     * Adds route to image
     *
     * @param  string $value
     * @return string
     */

    public function setValueAttribute(string $value): void
    {
        $this->attributes['value'] = Str::slug($value, '-');
    }

    /**
    * Get the question which owns answers
    */
    public function question()
    {
        return $this->belongsTo('App\Http\Modules\Questionnaires\Models\Question', 'questionnaires_questions_id');
    }
}
