<?php

namespace App\Http\Modules\Questionnaires\Models;

use Illuminate\Database\Eloquent\Model;

class UserAnswer extends Model
{
    protected $table = "questionnaires_users";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
//        'user_id', 'questionnaires_questions_id', 'answer', 'partial_round', 'complete_round'
    ];

    /**
     * The attributes that should be visible for arrays.
     *
     * @var array
     */
    protected $visible = [
        'id', 'user_id', 'questionnaires_questions_id', 'answer', 'partial_round', 'complete_round', 'created_at'
    ];

     /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
        'updated_at' => 'datetime:Y-m-d H:i:s'
    ];
}
