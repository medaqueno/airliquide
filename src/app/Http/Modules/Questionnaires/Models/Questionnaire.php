<?php

namespace App\Http\Modules\Questionnaires\Models;

use Illuminate\Database\Eloquent\Model;
use App\Http\Traits\HasTranslations;

class Questionnaire extends Model
{
    use HasTranslations;

    public $translatable = [
        'name'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'type'
    ];

    /**
     * The attributes that should be visible for arrays.
     *
     * @var array
     */
    protected $visible = [
        'id',
        'name',
        'type',
        'questions'
    ];

    /**
     * Get the questions for the questionnaires.
     */
    public function questions()
    {
        return $this->hasMany('App\Http\Modules\Questionnaires\Models\Question', 'questionnaires_id');
    }
}
