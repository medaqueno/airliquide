<?php

namespace App\Http\Modules\Questionnaires\Models;

use Illuminate\Database\Eloquent\Model;
use App\Http\Traits\HasTranslations;

class Question extends Model
{
    use HasTranslations;

    public $translatable = [
        'text'
    ];

    protected $table = "questionnaires_questions";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'text',
        'questionnaires_id',
        'therapies_id',
    ];

    /**
     * The attributes that should be visible for arrays.
     *
     * @var array
     */
    protected $visible = [
        'id',
        'text',
        'questionnaires_id',
        'therapies_id',
        'answers'
    ];

    /**
    * Get the parent questionnaire
    */
    public function questionnaire()
    {
        return $this->belongsTo('App\Http\Modules\Questionnaires\Models\Questionnaire', 'id');
    }

    /**
    * Get the related therapy
    */
    public function therapy()
    {
        return $this->belongsTo('App\Http\Modules\Therapies\Models\Therapy', 'id');
    }

    /**
     * Get the answers related
     */
    public function answers()
    {
        return $this->hasMany('App\Http\Modules\Questionnaires\Models\QuestionAnswer', 'questionnaires_questions_id');
    }
}
