<?php

namespace App\Http\Modules\Admin\Locale;

use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class SetLocaleController extends Controller
{
    public function __invoke(Request $request): RedirectResponse
    {
        Session()->put('locale', $request->input('lang'));

        return redirect()->back();
    }
}
