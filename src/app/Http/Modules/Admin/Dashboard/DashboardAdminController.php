<?php

namespace App\Http\Modules\Admin\Dashboard;

use App\Http\Controllers\Controller;

use Illuminate\View\View;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class DashboardAdminController extends Controller
{
    protected $now = '';

    public function __construct()
    {
        $this->now = Carbon::now()->endOfDay();
    }

    public function __invoke(): View
    {
        $this->getTherapiesCompleted();

        $stats = [
            'registeredUsersCurrentMonth' => $this->registeredUsersCurrentMonth(),
            'registeredUsersTotal' => $this->registeredUsersTotal(),
            'existingUsersCurrentMonth' => $this->existingUsersCurrentMonth(),
            'existingUsersTotal' => $this->existingUsersTotal(),
            'premsCompletedUsersCurrentMonth' => $this->premsCompletedUsersCurrentMonth(),
            'premsCompletedUsersTotal' => $this->premsCompletedUsersTotal(),
            'promsCompletedCurrentMonth' => $this->promsCompletedCurrentMonth(),
            'promsCompletedTotal' => $this->promsCompletedTotal(),
            'therapiesCompletedCurrentMonth' => $this->getTherapiesCompleted(true),
            'therapiesCompletedTotal' => $this->getTherapiesCompleted(),
            'therapiesUncompletedCurrentMonth' => $this->getTherapiesUncompleted(true),
            'therapiesUncompletedTotal' => $this->getTherapiesUncompleted(),
            'therapiesActiveCurrentMonth' => $this->therapiesActive(true),
            'therapiesActiveTotal' => $this->therapiesActive(),
        ];

        return view('admin.dashboard', ['stats' => $stats]);
    }

    private function registeredUsersCurrentMonth(): int
    {
        return DB::table('users')
        ->where('active', '=', 1)
        ->whereRaw('MONTH(created_at) = MONTH(CURRENT_DATE)')
        ->count();
    }

    private function registeredUsersTotal(): int
    {
        return DB::table('users')
        ->where('active', '=', 1)
        ->count();
    }

    private function existingUsersCurrentMonth(): int
    {
        return DB::table('users')
        ->whereRaw('MONTH(created_at) = MONTH(CURRENT_DATE)')
        ->count();
    }

    private function existingUsersTotal(): int
    {
        return DB::table('users')
        ->count();
    }

    private function premsCompletedUsersCurrentMonth(): int
    {
         return DB::table('questionnaires_users')
        ->join('questionnaires_questions', 'questionnaires_users.questionnaires_questions_id', '=', 'questionnaires_questions.id')
        ->where('questionnaires_id', '=', 4) // 4 = prem
        ->whereRaw('MONTH(questionnaires_users.created_at) = MONTH(CURRENT_DATE)')
        ->count();
    }

    private function premsCompletedUsersTotal(): int
    {
        return DB::table('questionnaires_users')
        ->join('questionnaires_questions', 'questionnaires_users.questionnaires_questions_id', '=', 'questionnaires_questions.id')
        ->where('questionnaires_id', '=', 4) // 4 = prem
        ->count();
    }

    private function promsCompletedCurrentMonth(): int
    {
         return DB::table('questionnaires_users')
        ->join('questionnaires_questions', 'questionnaires_users.questionnaires_questions_id', '=', 'questionnaires_questions.id')
        ->where('questionnaires_id', '=', 1) // 1 = prom
        ->whereRaw('MONTH(questionnaires_users.created_at) = MONTH(CURRENT_DATE)')
        ->count();
    }

    private function promsCompletedTotal(): int
    {
        return DB::table('questionnaires_users')
        ->join('questionnaires_questions', 'questionnaires_users.questionnaires_questions_id', '=', 'questionnaires_questions.id')
        ->where('questionnaires_id', '=', 1) // 1 = prom
        ->count();
    }

    private function getTherapiesCompleted(bool $month = false): int
    {
        // Get all entries from therapies_registries
        $therapies_registry = DB::table('therapies_registry')
                        ->selectRaw('therapies_registry.user_id, therapies_registry.therapies_id, therapies_registry.duration/60 as duration, therapies_users.posology_duration')
                        ->join('therapies_users', 'therapies_registry.therapies_id', '=', 'therapies_users.therapies_id')
                        ->whereRaw('ROUND(therapies_registry.duration/60) >= therapies_users.posology_duration')
                       ;

        if ($month === true) {
            $therapies_registry = $therapies_registry->whereRaw('MONTH(therapies_registry.created_at) = MONTH(CURRENT_DATE)');
        }

        return $therapies_registry->count();
    }

    private function getTherapiesUncompleted(bool $month = false): int
    {
        // Get all entries from therapies_registries
        $therapies_registry = DB::table('therapies_registry')
                        ->selectRaw('therapies_registry.user_id, therapies_registry.therapies_id, therapies_registry.duration/60 as duration, therapies_users.posology_duration')
                        ->join('therapies_users', 'therapies_registry.therapies_id', '=', 'therapies_users.therapies_id')
                        ->whereRaw('ROUND(therapies_registry.duration/60) < therapies_users.posology_duration')
                       ;

        if ($month === true) {
            $therapies_registry = $therapies_registry->whereRaw('MONTH(therapies_registry.created_at) = MONTH(CURRENT_DATE)');
        }

        return $therapies_registry->count();
    }

    private function therapiesActive(bool $month = false): int
    {
        // Get all entries from therapies_users
        $therapies_users = DB::table('therapies_users')
                        ->select(['therapies_id', 'posology_interval', 'start_date', 'end_date']);

        $therapies_users = $therapies_users->get()->toArray();

        // Calculate how many times each therapy should be done, and add them all
        return array_reduce($therapies_users, function ($total_times, $item) use ($month) {

            $item_start_date = Carbon::parse($item->start_date)->startOfDay();
            $item_end_date = Carbon::parse($item->end_date)->endOfDay();

            if ($month === true) {
                $start_date = $item_start_date > Carbon::now()->startOfMonth() ? $item_start_date : Carbon::now()->startOfMonth();
                $end_date = $item_end_date < $this->now ? $item_end_date : $this->now;
            } else {
                $start_date = $item_start_date;
                $end_date = $item_end_date > $this->now ? $this->now : $item_end_date;
            }

            $minutes_in_interval = ($item_start_date > $this->now || $item_end_date < $this->now) ? 0 : $end_date->diffInMinutes($start_date);

            $item_times = intval(ceil($minutes_in_interval/$item->posology_interval));
            $total_times += $item_times;

            return $total_times;
        }, 0);
    }
}
