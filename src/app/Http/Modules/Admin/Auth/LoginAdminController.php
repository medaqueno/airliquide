<?php

namespace App\Http\Modules\Admin\Auth;

use App\Http\Controllers\Controller;

use App\Http\Modules\Admin\Auth\Requests\LoginRequest;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\ThrottlesLogins;

class LoginAdminController extends Controller
{
    const USERCMS = 'vitalaireAdmin';
    const PASSWORDCMS = '$2y$10$KvHgxfiMNUYZ8qhbDMMtieFkcyyLk6vGdiVn0VW6s9cm9CR28Nbtm'; // Hash of password

    public function __construct()
    {
        $this->middleware('throttle:3,2');
    }

    public function __invoke(LoginRequest $request)
    {
        if ($request->input('username') === self::USERCMS && Hash::check($request->input('password'), self::PASSWORDCMS) === true) {
            $request->session()->put('authenticated', time());
            return redirect()->route('dash');
        }

        return redirect()->route('login')->with('status', __('auth.failed'));
    }
}
