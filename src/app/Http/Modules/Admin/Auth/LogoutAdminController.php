<?php

namespace App\Http\Modules\Admin\Auth;

use App\Http\Controllers\Controller;

use Illuminate\Http\RedirectResponse;

use Illuminate\Http\Request;

class LogoutAdminController extends Controller
{
    public function __invoke(Request $request)
    {
        $request->session()->flush();

        return redirect()->route('login');
    }
}
