<?php

namespace App\Http\Modules\Admin\Auth;

use App\Http\Controllers\Controller;

use Illuminate\View\View;

class LoginViewAdminController extends Controller
{
    public function __invoke(): View
    {
        return view('admin.login');
    }
}
