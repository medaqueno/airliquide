<?php

namespace App\Http\Modules\Legals;

use App\Http\Controllers\Controller;

use Illuminate\Http\Response;

use App\Http\Modules\Legals\Models\Legal;

class GetAllLegalsController extends Controller
{
    public function __invoke(): Response
    {
        // Only one row
        $response = Legal::find(1);

        return response($response);
    }
}
