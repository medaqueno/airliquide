<?php

namespace App\Http\Modules\Legals;

use App\Http\Controllers\Controller;
use Illuminate\View\View;
use Illuminate\Http\RedirectResponse;

use App\Http\Modules\Legals\GetAllLegalsController;

use App\Http\Modules\Legals\Requests\EditLegalsRequest;
use App\Http\Modules\Legals\Requests\CreateLegalsRequest;
use App\Http\Modules\Legals\Models\Legal;

class AdminResourceLegalsController extends Controller
{
    public function index(): View
    {
        $items = Legal::find(1);

        $items = $items === null ? ['legal' => '', 'privacy' => ''] : $items;

        return view('admin.legals_index', ['items' => $items]);
    }

    public function edit(EditLegalsRequest $request): RedirectResponse
    {
        // Only one row
         Legal::updateOrCreate(
             ['id' => 1],
             $request->all()
         );

        return redirect()->route('legals_index')->with('status', __('messages.save_sucess'));
    }
}
