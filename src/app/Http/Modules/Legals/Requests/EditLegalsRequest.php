<?php

namespace App\Http\Modules\Legals\Requests;

use App\BaseFormRequest;

class EditLegalsRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'privacy' => ['string'],
            'legal' => ['string']
        ];
    }
}
