<?php

namespace App\Http\Modules\Legals\Models;

use Illuminate\Database\Eloquent\Model;
use App\Http\Traits\HasTranslations;

class Legal extends Model
{
    use HasTranslations;

    public $translatable = [
        'privacy',
        'legal',
    ];

    protected $table = "legals";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
     protected $fillable = [
        'privacy',
        'legal',
     ];

    /**
     * The attributes that should be visible for arrays.
     *
     * @var array
     */
     protected $visible = [
        'privacy',
        'legal',
     ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
     protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
        'updated_at' => 'datetime:Y-m-d H:i:s',
     ];
}
