<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class NumSegSocial implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        // Si existe el número completo y tiene 12 dígitos, los descompone en a, b y c
        $na = substr($value, 0, 2);
        $nb = substr($value, 2, 8);
        $nc = substr($value, 10, 2);

        // Si el número es menor de 10 millones
        if ($nb < 10000000) {
            // Asignamos a d la suma de b+a * 10 millones
            $nd = $nb+$na*10000000;

            // Si el número es mayor de 10 millones
        } else {
            // Asignamos a d la concatenación de a y b
            $nd = $na.$nb;
        }

        // El código de validación ($c),
        // debe ser el resto de la división d entre 97
        $validacion = $nd % 97;

        return $validacion == $nc;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('errors.wrong_num_ss');
    }
}
