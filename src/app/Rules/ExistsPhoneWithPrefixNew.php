<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Http\Modules\Users\Models\User;

class ExistsPhoneWithPrefixNew implements Rule
{
    protected $params;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct(array $params)
    {
        $this->params = $params;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $found = User::where('phone', $value)
                    ->where('phone_prefix', $this->params['phone_prefix'])
                    ->first();

        if ($found) {
            return false;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('errors.existing_phoneNumber');
    }
}
