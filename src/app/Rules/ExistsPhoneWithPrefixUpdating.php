<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Http\Modules\Users\Models\User;

class ExistsPhoneWithPrefixUpdating implements Rule
{
    protected $params;

    protected $userId;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct(array $params, int $userId)
    {
        $this->userId = $userId;
        $this->params = $params;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $found = User::where('phone', $value)
                    ->where('phone_prefix', $this->params['phone_prefix'])
                    ->where('id', '!=', $this->userId)
                    ->first();

        if ($found) {
            return false;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('errors.existing_phoneNumber');
    }
}
