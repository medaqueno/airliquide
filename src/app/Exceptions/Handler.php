<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Throwable  $exception
     * @return void
     *
     * @throws \Exception
     */
    public function report(Throwable $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Throwable  $exception
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Throwable
     */
    public function render($request, Throwable $exception)
    {
        $class = get_class($exception);

        switch ($class) {
            case 'App\\Http\\Modules\\Auth\\Exceptions\\LoginException':
                return parent::render($request, $exception);
                break;

            case 'App\\Http\\Modules\\Auth\\Exceptions\\AuthException':
                return parent::render($request, $exception);
                break;

            case 'Illuminate\\Http\\Exceptions\\ThrottleRequestsException':
                if (route('login') === url()->current()) {
                    return redirect()->route('login')->with('status', __('auth.throttle', ['seconds' => 120]));
                }

                return $this->buildResponse('too_many_requests', $exception->getMessage(), 429);
                break;

            case 'Illuminate\\Auth\\AuthenticationException':
                return $this->buildResponse('auth_unauthenticated', __('errors.unauthenticated'), 401);
                break;

            case 'Symfony\\Component\\HttpKernel\\Exception\\NotFoundHttpException':
                return $this->buildResponse('url_not_found', $exception->getMessage(), 404);
                break;
            case 'App\\Http\\Modules\\Auth\\Exceptions\\CodeExpiredException':
                return parent::render($request, $exception);
                break;

            case 'App\\Http\\Modules\\Auth\\Exceptions\\ConfirmationException':
                return parent::render($request, $exception);
                break;
            case 'Illuminate\\Validation\\ValidationException':
                return parent::render($request, $exception);
                 break;
            case 'App\\Http\\Modules\\Auth\\Exceptions\\CodeVitalAireException':
                return parent::render($request, $exception);
                break;
            break;

            default:
                $devResponse = (env('APP_ENV') !== 'PRODUCTION' ? strip_tags($exception) : $exception->getMessage());
                return $this->buildResponse('generic.error', $exception->getMessage(), 500, $devResponse);
                break;
        }
    }

    private function buildResponse(string $errorCode, string $message, int $statusCode, string $devResponse = null)
    {
         return response()->json([
                'errors' => [
                    'error_code' => $errorCode,
                    'message' => $message,
                    'dev_response' => $devResponse
                ]
            ], $statusCode);
    }
}
