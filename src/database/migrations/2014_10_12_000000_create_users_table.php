<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();

            $table->string('name', 100);
            $table->string('surname', 100)->nullable();

            $table->string('phone_prefix', 32);
            $table->integer('phone')->unsigned()->unique();
            $table->string('email')->nullable()->unique();
            $table->string('code_vitalaire', 16)->unique();
            $table->bigInteger('num_ss')->unsigned()->unique();

            $table->boolean('legal_notifications');
            $table->boolean('legal_terms');

            $table->string('lang', 5)->nullable();
            $table->string('profile_img')->nullable();
            $table->boolean('active')->default(false);
            $table->string('password');
            $table->json('aux')->nullable();

            $table->rememberToken();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
