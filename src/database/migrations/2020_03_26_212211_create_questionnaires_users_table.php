<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuestionnairesUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questionnaires_users', function (Blueprint $table) {
            $table->id();

            $table->foreignId('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreignId('questionnaires_questions_id')->references('id')->on('questionnaires_questions')->onDelete('cascade');
            $table->foreignId('questionnaires_answers_id')->references('id')->on('questionnaires_answers')->onDelete('cascade');

            $table->integer('partial_round')->unsigned();
            $table->integer('complete_round')->unsigned();
            $table->json('aux')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::table('questionnaires_users', function (Blueprint $table) {
            $table->dropForeign('questionnaires_users_questionnaires_questions_id_foreign');
            $table->dropForeign('questionnaires_users_user_id_foreign');
            $table->dropForeign('questionnaires_users_questionnaires_answers_id_foreign');
         });

        Schema::dropIfExists('questionnaires_users');
    }
}
