<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuestionnairesQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questionnaires_questions', function (Blueprint $table) {
            $table->id();

            $table->foreignId('questionnaires_id')->references('id')->on('questionnaires')->onDelete('cascade');

            $table->text('text');

            $table->json('aux')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('questionnaires_questions', function (Blueprint $table) {
            $table->dropForeign('questionnaires_questions_questionnaires_id_foreign');
        });

        Schema::dropIfExists('questionnaires_questions');
    }
}
