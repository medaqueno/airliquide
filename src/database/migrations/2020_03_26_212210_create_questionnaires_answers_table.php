<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuestionnairesAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questionnaires_answers', function (Blueprint $table) {
            $table->id();

            $table->foreignId('questionnaires_questions_id')->references('id')->on('questionnaires_questions')->onDelete('cascade');

            $table->text('text');
            $table->text('value');

            $table->json('aux')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('questionnaires_answers', function (Blueprint $table) {
            $table->dropForeign('questionnaires_answers_questionnaires_questions_id_foreign');
        });

        Schema::dropIfExists('questionnaires_answers');
    }
}
