<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class InsertQuestionnaires extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('questionnaires')->insert([
                [
                    'id' => 1, 'name' => 'PROMs', 'type' => 'prom', 'aux' => null, 'created_at' => '2020-05-17 17:49:01', 'updated_at' => '2020-05-17 17:49:01'
                ],
                [
                    'id' => 4, 'name' => 'PREMs', 'type' => 'prem', 'aux' => null, 'created_at' => '2020-05-17 17:49:01', 'updated_at' => '2020-05-17 17:49:01'
                ]
            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
