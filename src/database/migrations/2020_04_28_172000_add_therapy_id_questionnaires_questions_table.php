<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTherapyIdQuestionnairesQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('questionnaires_questions', function (Blueprint $table) {
            $table->foreignId('therapies_id')->references('id')->on('therapies')->onDelete('cascade')->after('questionnaires_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::table('questionnaires_questions', function (Blueprint $table) {
            $table->dropForeign('questionnaires_questions_therapies_id_foreign');
         });
    }
}
