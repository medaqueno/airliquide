<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDefaultValuesUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropUnique('users_phone_unique');
            $table->dropUnique('users_num_ss_unique');

            $table->string('phone_prefix', 32)->nullable()->change();
            $table->integer('phone')->unsigned()->unique()->nullable()->change();

            $table->boolean('legal_notifications')->default(false)->change();
            $table->boolean('legal_terms')->default(false)->change();
            $table->bigInteger('num_ss')->unsigned()->unique()->nullable()->change();
            $table->string('password')->nullable()->change();
            $table->string('lang', 5)->default('es')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
        });
    }
}
