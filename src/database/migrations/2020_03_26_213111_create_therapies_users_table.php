<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTherapiesUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('therapies_users', function (Blueprint $table) {
            $table->id();

            $table->foreignId('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreignId('therapies_id')->references('id')->on('therapies')->onDelete('cascade');

            $table->string('data')->nullable();

            $table->dateTime('start_date')->nullable();
            $table->dateTime('end_date')->nullable();

            $table->string('units')->nullable();

            $table->string('posology_dosage')->nullable();
            $table->integer('posology_interval')->nullable();
            $table->integer('posology_duration')->nullable();

            $table->string('doctor')->nullable();
            $table->string('place')->nullable();

            $table->boolean('active')->default(false);

            $table->json('aux')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('therapies_users', function (Blueprint $table) {
            $table->dropForeign('therapies_users_user_id_foreign');
            $table->dropForeign('therapies_users_therapies_id_foreign');
        });

        Schema::dropIfExists('therapies_users');
    }
}
