<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTherapiesRegistryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('therapies_registry', function (Blueprint $table) {
            $table->id();

            $table->foreignId('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreignId('therapies_id')->references('id')->on('therapies')->onDelete('cascade');

            $table->dateTime('start_time');
            $table->dateTime('end_time');
            $table->integer('duration')->unsigned();

            $table->json('aux')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('therapies_registry', function (Blueprint $table) {
            $table->dropForeign('therapies_registry_therapies_id_foreign');
            $table->dropForeign('therapies_registry_user_id_foreign');
        });

        Schema::dropIfExists('therapies_registry');
    }
}
