<html>
<head></head>
<body>
    <h3>Sugerencia Recibida</h3>

    <p>
        <strong>Usuario</strong><br>
        ID: {{ $user->id }}<br>
        Teléfono: {{ $user->phone }}<br>
        Email (si existe): {{ $user->email }}<br>
        Identificador Vitalaire: {{ $user->code_vitalaire }}<br>
        Nombre: {{ $user->name }} {{ $user->surname }}
    </p>

    <p>
        <strong>Sugerencia:</strong><br>
        {{ $suggestion->text }}
    </p>
</body>
</html>