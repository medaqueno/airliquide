@extends('admin.layout_main')

@section('content')

          <div class="level">
            <div class="level-left">
              <div class="level-item">
                <div class="title">Estadísticas</div>
              </div>
            </div>

          </div>

          <div class="columns is-multiline">
            <div class="column">

              <div class="box">
                <br><br>
                <div class="level">
                  <div class="level-item">
                    <div class="has-text-centered">
                      <div class="heading">Usuarios registrados<br>mes actual / totales</div>
                      <div class="title is-4">
                        {{ $stats['registeredUsersCurrentMonth'] }} / {{ $stats['registeredUsersTotal'] }}
                      </div>
                    </div>
                  </div>
                  <div class="level-item">
                    <div class="has-text-centered">
                      <div class="heading">Usuarios volcados a la BBDD<br>mes actual / totales</div>
                      <div class="title is-4">
                        {{ $stats['existingUsersCurrentMonth'] }} / {{ $stats['existingUsersTotal'] }}
                      </div>
                    </div>
                  </div>
                  <div class="level-item">
                    <div class="has-text-centered">
                      <div class="heading">Nº PREMS respondidos<br>mes actual / totales</div>
                      <div class="title is-4">
                        {{ $stats['premsCompletedUsersCurrentMonth'] }} / {{ $stats['premsCompletedUsersTotal'] }}
                      </div>
                    </div>
                  </div>
                  <div class="level-item">
                    <div class="has-text-centered">
                      <div class="heading">Nº PROMS respondidos<br>mes actual / totales</div>
                      <div class="title is-4">
                        {{ $stats['promsCompletedCurrentMonth'] }} / {{ $stats['promsCompletedTotal'] }}
                      </div>
                    </div>
                  </div>
                </div>

                <br><br>


                <div class="level">
                  <div class="level-item">
                    <div class="has-text-centered">
                      <div class="heading">Terapias completadas<br>mes actual / totales</div>
                      <div class="title is-4">
                        {{ $stats['therapiesCompletedCurrentMonth'] }} / {{ $stats['therapiesCompletedTotal'] }}
                      </div>
                    </div>
                  </div>
                  <div class="level-item">
                    <div class="has-text-centered">
                      <div class="heading">Terapias incompletas<br>mes actual / totales</div>
                      <div class="title is-4">
                        {{ $stats['therapiesUncompletedCurrentMonth'] }} / {{ $stats['therapiesUncompletedTotal'] }}
                      </div>
                    </div>
                  </div>
                  <div class="level-item">
                    <div class="has-text-centered">
                      <div class="heading">Terapias activas<br>mes actual / totales<!--<br>(con fecha de inicio/fin dentro del intervalo)--></div>
                      <div class="title is-4">
                        {{ $stats['therapiesActiveCurrentMonth'] }} / {{ $stats['therapiesActiveTotal'] }}
                      </div>
                    </div>
                  </div>
                </div>
                <br><br>
              </div>

            </div>
          </div>



@endsection