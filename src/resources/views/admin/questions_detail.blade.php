@extends('admin.layout_main')

@section('content')

          <div class="level">
            <div class="level-left">
              <div class="level-item">
                <div class="title">Cuestionarios -> Preguntas</div>
              </div>
            </div>
          </div>

          <div class="columns is-multiline">
            <div class="column">

              <div class="content">
                @if (session('status'))

                <div class="columns">
                  <div class="column">
                    <div class="notification is-success is-light is-12">
                        {{ session('status') }}
                      </div>
                  </div>

                </div>

                @endif

                  <form action="{{ route('questions_edit', ['id' =>  $item->id]) }}" method="post" class="box" enctype="multipart/form-data">

                    <div class="level">
                      <!-- Left side -->
                      <div class="level-left">
                        <div class="level-item">
                          <h5 class="subtitle is-5">ID: {{ $item->id }}</h5>
                        </div>
                      </div>

                      <div class="level-right">
                        <div class="level-item">
                          <a href="{{ route('answers_index', ['question_id' => $item->id]) }}" class="button is-link is-light">Ver respuestas</a>
                        </div>
                      </div>
                    </div>

                    <div class="columns is-multiline">

                      <div class="column">
                        <div class="field">
                          <label class="label">Texto</label>
                          <div class="control">
                            <textarea name="text" class="textarea">{!! old('text') ?? $item->text !!}</textarea>
                            @error('text')
                            <p class="help is-danger">{{ $message }}</p>
                            @enderror
                          </div>
                        </div>

                        <br><br>

                        <div class="level">
                          <!-- Left side -->
                          <div class="level-left">
                          <h5 class="subtitle is-5">Común en todos los idiomas</h5>
                          </div>
                        </div>

                        <div class="field">
                          <div class="control">
                            <label class="label">Tipo</label>
                            <div class="select">
                               @php
                                $questionnaires_id = old('questionnaires_id') ?? $item->questionnaires_id;
                              @endphp
                            <select name="questionnaires_id">
                              <option value="1" @if($questionnaires_id  == 1) selected @endif>PROMs</option>
                              <option value="4" @if($questionnaires_id  == 4) selected @endif>PREMs</option>
                            </select>
                            @error('questionnaires_id')
                              <p class="help is-danger">{{ $message }}</p>
                            @enderror
                            <br>
                          </div>
                          </div>
                        </div>

                        <div class="field">
                          <div class="control">
                            <label class="label">Terapia Asociada</label>
                            <div class="select">
                               @php
                                $therapies_id = old('therapies_id') ?? $item->therapies_id;
                              @endphp
                            <select name="therapies_id">
                              @foreach ($therapies as $therapy)
                                <option value="{{ $therapy->id }}" @if($therapies_id  == $therapy->id) selected @endif>{{ $therapy->name }}</option>
                              @endforeach
                            </select>
                            @error('therapies_id')
                              <p class="help is-danger">{{ $message }}</p>
                            @enderror
                            <br>
                          </div>
                          </div>
                        </div>

                      </div>

                    </div>

                    <br><br>

                    <div class="level">
                      <!-- Left side -->
                      <div class="level-left">

                        <div class="level-item">
                          <div class="field is-grouped">
                            <div class="control">
                              <a href="{{ route('questions_delete', ['id' => $item->id]) }}" class="button is-danger"><span class="icon is-medium is-left"><i class="fas fa-trash-alt"></i></span>
                                <span class="is-size-7">(incluye todos los idiomas)</span>
                              </a>
                            </div>
                          </div>
                        </div>

                      </div>

                      <!-- Right side -->
                      <div class="level-right">
                        <div class="level-item">
                          <div class="field is-grouped">
                            <div class="control">
                              <button type="submit" class="button is-link">Guardar</button>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                  </form>
              </div>



            </div>
          </div>

          <div class="level">
              <!-- Left side -->
              <div class="level-left"></div>

              <!-- Right side -->
              <div class="level-right">
                <div class="level-item">
                  <div class="field is-grouped">
                    <div class="control">
                      <a href="{{ route('questions_index') }}" class="button is-link is-light">Volver</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>


@endsection