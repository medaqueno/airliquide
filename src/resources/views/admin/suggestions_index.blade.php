@extends('admin.layout_main')

@section('content')

          <div class="level">
            <div class="level-left">
              <div class="level-item">
                <div class="title">Sugerencias</div>
              </div>
            </div>
          </div>

            <div class="columns is-multiline">
              <div class="column">

                <table class="table is-bordered is-striped is-hoverable is-fullwidth">
                  <thead>
                    <tr>
                      <th class="has-text-centered">ID</th>
                      <th>Texto</th>
                      <th>Usuario</th>
                      <th>Fecha</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>

                    @foreach ($suggestions as $item)

                      <tr>
                        <td class="has-text-centered">{{ $item->id }}</td>
                        <td>{{ \Illuminate\Support\Str::limit($item->text, 200, $end='...') }}</td>
                        <td>
                          @if($item->user)
                              ID: <strong>{{ $item->user->id }}</strong>
                              <br>
                              {{ $item->user->email }}
                          @endif
                        </td>
                         <td>{{ $item->created_at }}</td>
                        <td class="has-text-centered">
                          <a href="{{ route('suggestions_detail', ['id' => $item->id]) }}" class="button"><span class="icon is-medium is-left"><i class="fas fa-eye"></i></span></a>
                        </td>

                      </tr>

                    @endforeach
                  </tbody>
                </table>

                <nav class="pagination" role="navigation" aria-label="pagination">
                   {{ $suggestions->links('vendor.pagination.bulma') }}
                </nav>

              </div>
            </div>


@endsection