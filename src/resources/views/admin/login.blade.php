<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Admin Vitalaire</title>

        <!-- Fonts -->
        <!--<link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">-->

        <!-- Styles -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.8.2/css/bulma.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
    </head>
    <body>
        <section class="hero is-fullheight">
          <div class="hero-body">
            <div class="container">
              <div class="columns is-centered">

                <div class="column is-5-tablet is-5-desktop is-4-widescreen">

                  <div class="column has-text-centered">
                    <img src="/img/logo.png" style="max-width: 60%;" />
                  </div>

                  <form action="/adm" method="post" class="box">

                    @csrf

                    @if (session('status'))
                        <div class="notification is-danger is-light">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="field">
                      <label class="label">Usuario</label>
                      <div class="control has-icons-left has-icons-right">
                        <input name="username" class="input @if($errors->has('password')) is-danger @endif" type="text" placeholder="Usuario" value=""> <!-- {{ old('username') }} -->
                        <span class="icon is-small is-left">
                          <i class="fas fa-user"></i>
                        </span>
                       @error('username')
                        <span class="icon is-small is-right">
                          <i class="fas fa-exclamation-triangle"></i>
                        </span>
                        @enderror
                      </div>
                      @error('username')
                      <p class="help is-danger">{{ $message }}</p>
                      @enderror
                    </div>

                    <div class="field">
                      <label class="label">Contraseña</label>
                      <div class="control has-icons-left has-icons-right">
                        <input name="password" class="input @if($errors->has('password')) is-danger @endif" type="password" placeholder="*******" value=""> <!-- {{ old('password') }} -->
                        <span class="icon is-small is-left">
                          <i class="fas fa-lock"></i>
                        </span>
                        @error('password')
                        <span class="icon is-small is-right">
                          <i class="fas fa-exclamation-triangle"></i>
                        </span>
                        @enderror
                      </div>
                      @error('password')
                      <p class="help is-danger">{{ $message }}</p>
                      @enderror
                    </div>


                    <div class="field has-text-right">
                      <button class="button">
                        Login
                      </button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </section>

    </body>
</html>
