@extends('admin.layout_main')

@section('content')

          <div class="level">
            <div class="level-left">
              <div class="level-item">
                <div class="title">Cuestionarios -> Preguntas -> Respuestas</div>
              </div>
            </div>
          </div>

          <div class="columns is-multiline">
            <div class="column">

              <div class="content">
                @if (session('status'))

                <div class="columns">
                  <div class="column">
                    <div class="notification is-success is-light is-12">
                        {{ session('status') }}
                      </div>
                  </div>

                </div>

                @endif

                  <form action="{{ route('answers_edit', ['question_id' => $question_id, 'answer_id' => $answer_id]) }}" method="post" class="box" enctype="multipart/form-data">

                    <div class="level">
                      <!-- Left side -->
                      <div class="level-left">
                        <h5 class="subtitle is-5">ID: {{ $item->id }}</h5>
                      </div>
                    </div>

                    <div class="columns is-multiline">
                      <div class="column">

                        <!--<input type="hidden" name="questionnaires_questions_id" value="{{ $question_id }}">-->

                        <div class="field">
                          <label class="label">Texto</label>
                          <div class="control">
                            <input type="text" name="text" class="input" value="{!! old('text') ?? $item->text !!}">
                            @error('text')
                            <p class="help is-danger">{{ $message }}</p>
                            @enderror
                          </div>
                        </div>

                        <br><br>

                        <div class="level">
                          <!-- Left side -->
                          <div class="level-left">
                          <h5 class="subtitle is-5">Común en todos los idiomas</h5>
                          </div>
                        </div>

                        <div class="field">
                          <label class="label">Valor (será convertido a slug: valor-convertido-slug)</label>
                          <div class="control">
                            <input type="text" name="value" class="input" value="{!! old('value') ?? $item->value !!}">
                            @error('value')
                            <p class="help is-danger">{{ $message }}</p>
                            @enderror
                          </div>
                        </div>

                      </div>
                    </div>

                    <br><br>

                    <div class="level">
                      <!-- Left side -->
                      <div class="level-left">
                        <div class="level-item">
                          <div class="field is-grouped">
                            <div class="control">
                              <a href="{{ route('answers_delete', ['question_id' => $question_id, 'answer_id' => $answer_id]) }}" class="button is-danger"><span class="icon is-medium is-left"><i class="fas fa-trash-alt"></i></span>
                                <span class="is-size-7">(incluye todos los idiomas)</span>
                              </a>
                            </div>
                          </div>
                        </div>
                      </div>

                      <!-- Right side -->
                      <div class="level-right">
                        <div class="level-item">
                          <div class="field is-grouped">
                            <div class="control">
                              <button type="submit" class="button is-link">Guardar</button>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                  </form>
              </div>

            </div>
          </div>

          <div class="level">
              <!-- Left side -->
              <div class="level-left"></div>

              <!-- Right side -->
              <div class="level-right">
                <div class="level-item">
                  <div class="field is-grouped">
                    <div class="control">
                      <a href="{{ route('answers_index', ['question_id' => $question_id]) }}" class="button is-link is-light">Volver</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>


@endsection