@extends('admin.layout_main')

@section('content')

          <div class="level">
            <div class="level-left">
              <div class="level-item">
                <div class="title">FAQs</div>
              </div>
            </div>
          </div>

          <div class="columns is-multiline">
            <div class="column">

              <div class="content">
                @if (session('status'))

                <div class="columns">
                  <div class="column">
                    <div class="notification is-success is-light is-12">
                        {{ session('status') }}
                      </div>
                  </div>

                </div>

                @endif


                  <form action="{{ route('faqs_store') }}" method="post" class="box">

                    <div class="level">
                      <!-- Left side -->
                      <div class="level-left">
                        <h5 class="subtitle is-5">Crear nuevo</h5>
                      </div>
                    </div>

                    <div class="columns is-multiline">
                      <div class="column">
                        <div class="field">
                          <label class="label">Título</label>
                          <div class="control">
                            <textarea name="title" class="textarea">{{ old('title') }}</textarea>
                            @error('title')
                            <p class="help is-danger">{{ $message }}</p>
                            @enderror
                          </div>

                        </div>

                        <div class="field">
                          <label class="label">Texto</label>
                          <div class="control">
                            <textarea name="text" class="ckeditor textarea">{{ old('text') }}</textarea>
                            @error('text')
                            <p class="help is-danger">{{ $message }}</p>
                            @enderror
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="level">
                      <!-- Left side -->
                      <div class="level-left"></div>

                      <!-- Right side -->
                      <div class="level-right">
                        <div class="level-item">
                          <div class="field is-grouped">
                            <div class="control">
                              <button type="submit" class="button is-link">Guardar</button>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                  </form>
              </div>



            </div>
          </div>

          <div class="level">
              <!-- Left side -->
              <div class="level-left"></div>

              <!-- Right side -->
              <div class="level-right">
                <div class="level-item">
                  <div class="field is-grouped">
                    <div class="control">
                      <a href="{{ route('faqs_index') }}" class="button is-link is-light">Volver</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>


@endsection