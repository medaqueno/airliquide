@extends('admin.layout_main')

@section('content')

          <div class="level">
            <div class="level-left">
              <div class="level-item">
                <div class="title">FAQs</div>
              </div>
            </div>
          </div>

            <div class="columns is-multiline">
              <div class="column">

                <div class="level">
                  <!-- Left side -->
                  <div class="level-left">
                    <div class="level-item">
                      <div class="field is-grouped">
                        <div class="control">
                          <a href="{{ route('faqs_create') }}" class="button is-link is-light">Crear nuevo</a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <table class="table is-bordered is-striped is-hoverable is-fullwidth">
                  <thead>
                    <tr>
                      <th class="has-text-centered">ID</th>
                      <th>Título</th>
                      <th>Texto</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>

                    @foreach ($items as $item)

                      <tr>
                        <td class="has-text-centered">{{ $item->id }}</td>
                        <td>{{ $item->title }}</td>
                        <td>{!! \Illuminate\Support\Str::limit($item->text, 200, $end='...') !!}</td>
                        <td class="has-text-centered">
                          <a href="{{ route('faqs_detail', ['id' => $item->id]) }}" class="button"><span class="icon is-medium is-left"><i class="fas fa-edit"></i></span></a>
                        </td>
                      </tr>

                    @endforeach
                  </tbody>
                </table>

                <nav class="pagination" role="navigation" aria-label="pagination">
                   {{ $items->links('vendor.pagination.bulma') }}
                </nav>

              </div>
            </div>


@endsection