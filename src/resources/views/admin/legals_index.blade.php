@extends('admin.layout_main')

@section('content')

          <div class="level">
            <div class="level-left">
              <div class="level-item">
                <div class="title">Legales</div>
              </div>
            </div>
          </div>

          @if (session('status'))

                <div class="columns">
                  <div class="column">
                    <div class="notification is-success is-light is-12">
                        {{ session('status') }}
                      </div>
                  </div>

                </div>

          @endif

          <form action="{{ route('legals_edit') }}" method="post" class="box">

            <div class="columns is-multiline">
              <div class="column">
                <div class="field">
                  <label class="label">Legal</label>
                  <div class="control">
                    <textarea name="legal" class="ckeditor textarea">{!! old('legal') ?? $items['legal'] !!}</textarea>
                  </div>
                </div>
              </div>
            </div>

            <div class="columns is-multiline">
              <div class="column">
                <div class="field">
                  <label class="label">Política de privacidad</label>
                  <div class="control">
                    <textarea name="privacy" class="ckeditor textarea">{!! old('privacy') ?? $items['privacy'] !!}</textarea>
                  </div>
                </div>
              </div>
            </div>

            <div class="level">
              <!-- Left side -->
              <div class="level-left"></div>

              <!-- Right side -->
              <div class="level-right">
                <div class="level-item">
                  <div class="field is-grouped">
                    <div class="control">
                      <button type="submit" class="button is-link">Guardar</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </form>


@endsection