@extends('admin.layout_main')

@section('content')

          <div class="level">
            <div class="level-left">
              <div class="level-item">
                <div class="title">Sugerencias</div>
              </div>
            </div>
          </div>

          <div class="columns is-multiline">
            <div class="column">

              <div class="content">
                @if($suggestion->user)
                <h4 class="subtitle is-4">Usuario</h4>
                 <p>
                  ID: {{ $suggestion->user->id }}
                  <br>
                  {{ $suggestion->user->name }} {{ $suggestion->user->surname }}
                  <br>
                  @if($suggestion->user->email)
                    <a href="mailto:{{ $suggestion->user->email }}">{{ $suggestion->user->email }}</a>
                  @endif
                @endif
              </p>
              <h4 class="subtitle is-4">Sugerencia:</h4>
              <p>
                {{ $suggestion->text }}
              </p>
              </div>



            </div>
          </div>

          <div class="level">
              <!-- Left side -->
              <div class="level-left"></div>

              <!-- Right side -->
              <div class="level-right">
                <div class="level-item">
                  <div class="field is-grouped">
                    <div class="control">
                      <a href="{{ route('suggestions_index') }}" class="button is-link is-light">Volver</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>


@endsection