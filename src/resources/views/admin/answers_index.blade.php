@extends('admin.layout_main')

@section('content')

          <div class="level">
            <div class="level-left">
              <div class="level-item">
                <div class="title">Cuestionarios -> Preguntas -> Respuestas</div>
              </div>
            </div>
          </div>

            <div class="columns is-multiline">
              <div class="column">

                <div class="level">
                  <!-- Left side -->
                  <div class="level-left">
                    <div class="level-item">
                      <h6 class="subtitle is-6">Pregunta: {{ $question->text }}</h6>
                    </div>
                  </div>
                  <div class="level-right">
                    <div class="level-item">
                      <a href="{{ route('answers_create', ['question_id' => $question_id]) }}" class="button is-link is-light">Crear nueva respuesta</a>
                    </div>
                  </div>
                </div>

                <table class="table is-bordered is-striped is-hoverable is-fullwidth">
                  <thead>
                    <tr>
                      <th class="has-text-centered">ID</th>
                      <th>Texto</th>
                      <th>Valor</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>

                    @foreach ($items as $item)

                      <tr>
                        <td class="has-text-centered">{{ $item->id }}</td>
                        <td>{!! \Illuminate\Support\Str::limit($item->text, 200, $end='...') !!}</td>
                        <td>{{ $item->value }} </td>
                        <td class="has-text-centered">
                          <a href="{{ route('answers_detail', ['question_id' => $question_id, 'answer_id' => $item->id]) }}" class="button"><span class="icon is-medium is-left"><i class="fas fa-edit"></i></span></a>
                        </td>
                      </tr>

                    @endforeach
                  </tbody>
                </table>

                <nav class="pagination" role="navigation" aria-label="pagination">
                   {{ $items->links('vendor.pagination.bulma') }}
                </nav>

              </div>
            </div>

            <div class="level">
              <!-- Left side -->
              <div class="level-left"></div>

              <!-- Right side -->
              <div class="level-right">
                <div class="level-item">
                  <div class="field is-grouped">
                    <div class="control">
                      <a href="{{ route('questions_index') }}" class="button is-link is-light">Volver</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>


@endsection