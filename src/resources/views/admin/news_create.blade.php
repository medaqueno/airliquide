@extends('admin.layout_main')

@section('content')

          <div class="level">
            <div class="level-left">
              <div class="level-item">
                <div class="title">Noticias</div>
              </div>
            </div>
          </div>

          <div class="columns is-multiline">
            <div class="column">

              <div class="content">
                @if (session('status'))

                <div class="columns">
                  <div class="column">
                    <div class="notification is-success is-light is-12">
                        {{ session('status') }}
                      </div>
                  </div>

                </div>

                @endif

                  <form action="{{ route('news_store') }}" method="post" class="box" enctype="multipart/form-data">

                    <div class="level">
                      <!-- Left side -->
                      <div class="level-left">
                        <h5 class="subtitle is-5">Crear Nuevo</h5>
                      </div>
                    </div>

                    <div class="columns is-multiline">
                      <div class="column">
                        <div class="field">
                          <label class="label">Título</label>
                          <div class="control">
                            <input type="text" name="title" class="input" value="{{ old('title')  }}">

                            @error('title')
                            <p class="help is-danger">{{ $message }}</p>
                            @enderror
                          </div>
                        </div>

                        <div class="field">
                          <label class="label">Intro</label>
                          <div class="control">
                            <textarea name="intro" class="ckeditor textarea">{!! old('intro') !!}</textarea>
                            @error('intro')
                            <p class="help is-danger">{{ $message }}</p>
                            @enderror
                          </div>
                        </div>

                         <div class="field">
                          <label class="label">Texto</label>
                          <div class="control">
                            <textarea name="text" class="ckeditor textarea">{!! old('text') !!}</textarea>
                            @error('text')
                            <p class="help is-danger">{{ $message }}</p>
                            @enderror
                          </div>
                        </div>


                        <br><br>

                        <div class="level">
                          <!-- Left side -->
                          <div class="level-left">
                          <h5 class="subtitle is-5">Común en todos los idiomas</h5>
                          </div>
                        </div>

                        <div class="field">
                          <div class="control">
                            <label class="label">Activo</label>
                            <div class="select">
                               @php
                                $active = old('active');
                              @endphp
                            <select name="active">
                              <option value="0" @if($active  == 0) selected @endif>No</option>
                              <option value="1" @if($active  == 1) selected @endif>Sí</option>
                            </select>
                            @error('active')
                              <p class="help is-danger">{{ $message }}</p>
                            @enderror
                            <br>
                          </div>
                          </div>
                        </div>


                        <div class="field">
                          <label class="label">Fecha</label>
                          <div class="control">
                            <input type="text" name="date" id="date_field" value="{{ old('date')  }}" readonly="readonly">
                            @error('date')
                            <p class="help is-danger">{{ $message }}</p>
                            @enderror
                          </div>
                        </div>

                        <script>
                          const options = {
                            "type": "datetime",
                            "showClearButton": false,
                            "showHeader": false,
                            "showTodayButton": false,
                            "dateFormat": "DD-MM-YYYY",
                          }
                          // Initialize all input of date type.
                          const calendars = bulmaCalendar.attach('#date_field', options)

                          // Loop on each calendar initialized
                          calendars.forEach(calendar => {
                            // Add listener to date:selected event
                            calendar.on('date:selected', date => {
                              console.log(date)
                            })
                          })
                        </script>
                        <style>
                          .datetimepicker-clear-button {
                            display: none !important;
                          }
                        </style>

                        <label class="label">Imagen (máx 4mb)</label>

                        <div id="file-image" class="file has-name">

                          <label class="file-label">
                            <input class="file-input" type="file" name="image">
                            <span class="file-cta">
                              <span class="file-icon">
                                <i class="fas fa-upload"></i>
                              </span>
                              <span class="file-label">
                                Selecciona una imagen
                              </span>
                            </span>
                          </label>
                        </div>

                        <br><br>

                        <script>
                          const fileInput = document.querySelector('#file-image input[type=file]');
                          fileInput.onchange = () => {
                            if (fileInput.files.length > 0) {
                              const fileName = document.querySelector('#file-image .file-name');
                              fileName.textContent = fileInput.files[0].name;
                            }
                          }
                        </script>

                      </div>
                    </div>

                    <div class="level">
                      <!-- Left side -->
                      <div class="level-left"></div>

                      <!-- Right side -->
                      <div class="level-right">
                        <div class="level-item">
                          <div class="field is-grouped">
                            <div class="control">
                              <button type="submit" class="button is-link">Guardar</button>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                  </form>
              </div>



            </div>
          </div>

          <div class="level">
              <!-- Left side -->
              <div class="level-left"></div>

              <!-- Right side -->
              <div class="level-right">
                <div class="level-item">
                  <div class="field is-grouped">
                    <div class="control">
                      <a href="{{ route('news_index') }}" class="button is-link is-light">Volver</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>


@endsection