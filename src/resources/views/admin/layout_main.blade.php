<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Admin Vitalaire</title>

        <!-- Fonts -->
        <!--<link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">-->

        <!-- Styles -->
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.8.2/css/bulma.min.css">
        <link rel="stylesheet" type="text/css"  href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">

        <script src="//cdn.ckeditor.com/4.14.0/basic/ckeditor.js"></script>

        <link rel="stylesheet" type="text/css"  href="https://cdn.jsdelivr.net/npm/bulma-calendar@6.0.7/dist/css/bulma-calendar.min.css">
        <script src="https://cdn.jsdelivr.net/npm/bulma-calendar@6.0.7/dist/js/bulma-calendar.min.js"></script>
    </head>
    <body>

      <div class="section" style="padding-top: 20px;">
        <div class="columns">
          <aside class="column is-2">

            <a href="{{ route('dash') }}"><img src="/img/logo.png" style="max-width: 60%;" /></a>

            <nav class="menu">
              <p class="menu-label">
                General
              </p>
              <ul class="menu-list">
                <li><a href="{{ route('dash') }}">Estadísticas</a></li>
                <li><a href="{{ route('legals_index') }}">Legales</a></li>
                <li><a href="{{ route('suggestions_index') }}">Sugerencias</a></li>
                <li><a href="{{ route('faqs_index') }}">FAQs</a></li>
                <li><a href="{{ route('news_index') }}">Noticias</a></li>
                <li><a href="{{ route('questions_index') }}">Cuestionarios</a></li>
              </ul>
              <p class="menu-label">
                <br>
              </p>
              <ul class="menu-list">
                <li><a href="{{ route('logout') }}">Cerrar sesión</a></li>
              </ul>
            </nav>
          </aside>

          <main class="column">

            <div class="level">
              <div class="level-left"></div>
              <div class="level-right">
                <div class="level-item">
                   <label class="label is-small" style="margin-right: 0.5em;">Idioma Contenido: </label>
                  <div class="control has-icons-left">
                    <div class="select is-small">
                      @php
                        $locale = \App::getLocale();
                      @endphp
                      <select name="lang" onChange="location.href='{{ route('set_locale') }}?lang='+this.value+'&callback=' + location.href">
                        <option value="es" @if($locale  === 'es') selected @endif>Castellano</option>
                        <option value="cat" @if($locale  === 'cat') selected @endif>Catalá</option>
                      </select>
                    </div>
                    <span class="icon is-medium is-left">
                      <i class="fas fa-globe"></i>
                    </span>
                  </div>
                </div>
              </div>
            </div>

            @yield('content')
          </main>
        </div>
      </div>

    </body>

</html>
