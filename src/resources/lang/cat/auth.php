<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'auth_error' => 'Error de Autenticación',
    'failed' => 'Estas credenciales no coinciden con nuestros registros.',
    'throttle' => 'Demasiados intentos de acceso. Por favor inténtelo de nuevo en :seconds segundos.',
    'sms_message' => 'Por favor introduzca el siguiente código para continuar con el registro en Vitalaire: :code',
    'change_password' => 'Ha solicitado un cambio de contraseña en Vitalaire. La nueva contraseña es: :changePassword'
];
