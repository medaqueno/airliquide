<?php

return [

    'confirmation_register_ko' => 'La confirmación de contraseña es incorrecta.',
    'confirmation_sms_ko' => 'La confirmación del código enviado por SMS es incorrecta.',
    'confirmation_expired' => 'El código enviado ha expirado.',
    'unauthenticated' => 'Unauthenticated',
    'existing_phoneNumber' => 'El número de teléfono ya existe.',
    'wrong_num_ss' => 'Número de la Seguridad Social no válido',
    'code_vitalaire_not_found' => 'El código de Vitalaire no existe.'
];
