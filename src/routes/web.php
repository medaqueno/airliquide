<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware('auth:sanctum')->get('/health', function (Request $request) {
    return 'OK logged';
});

Route::get('/healthas', function () {
    return 'OK';
});


Route::group(array('prefix' => 'adm'), function () {

        // Views
        Route::get('/', 'Admin\Auth\LoginViewAdminController')->name('login');
        Route::post('/', 'Admin\Auth\LoginAdminController');
        Route::get('/logout', 'Admin\Auth\LogoutAdminController')->name('logout');

        Route::middleware(['authCMS'])->group(function () {

            Route::get('/setLocale', 'Admin\Locale\SetLocaleController')->name('set_locale');

            Route::get('/dash', 'Admin\Dashboard\DashboardAdminController')->name('dash');

            Route::get('/legals', 'Legals\AdminResourceLegalsController@index')->name('legals_index');
            Route::post('/legals', 'Legals\AdminResourceLegalsController@edit')->name('legals_edit');

            Route::get('/suggestions', 'Suggestions\AdminResourceSuggestionsController@index')->name('suggestions_index');
            Route::get('/suggestions/{id}', 'Suggestions\AdminResourceSuggestionsController@show')->where('id', '[0-9]+')->name('suggestions_detail');

            Route::get('/faqs/create', 'Faqs\AdminResourceFaqsController@create')->name('faqs_create');
            Route::post('/faqs/create', 'Faqs\AdminResourceFaqsController@store')->name('faqs_store');
            Route::get('/faqs', 'Faqs\AdminResourceFaqsController@index')->name('faqs_index');
            Route::get('/faqs/{id}', 'Faqs\AdminResourceFaqsController@show')->where('id', '[0-9]+')->name('faqs_detail');
            Route::patch('/faqs/{id}', 'Faqs\AdminResourceFaqsController@edit')->where('id', '[0-9]+')->name('faqs_edit');
            Route::get('/faqs/delete/{id}', 'Faqs\AdminResourceFaqsController@delete')->where('id', '[0-9]+')->name('faqs_delete');

            Route::get('/news', 'News\AdminResourceNewsController@index')->name('news_index');
            Route::get('/news/create', 'News\AdminResourceNewsController@create')->name('news_create');
            Route::post('/news/create', 'News\AdminResourceNewsController@store')->name('news_store');
            Route::get('/news/{id}', 'News\AdminResourceNewsController@show')->where('id', '[0-9]+')->name('news_detail');

            Route::post('/news/{id}', 'News\AdminResourceNewsController@edit')->where('id', '[0-9]+')->name('news_edit');

            Route::get('/news/delete/{id}', 'News\AdminResourceNewsController@delete')->where('id', '[0-9]+')->name('news_delete');


            Route::get('/questionnaires/questions', 'Questionnaires\AdminQuestionnairesController@index')->name('questions_index');
            Route::get('/questionnaires/questions/create', 'Questionnaires\AdminQuestionnairesController@create')->name('questions_create');
            Route::post('/questionnaires/questions/create', 'Questionnaires\AdminQuestionnairesController@store')->name('questions_store');
            Route::get('/questionnaires/questions/{id}', 'Questionnaires\AdminQuestionnairesController@show')->where('id', '[0-9]+')->name('questions_detail');
            Route::post('/questionnaires/questions/{id}', 'Questionnaires\AdminQuestionnairesController@edit')->where('id', '[0-9]+')->name('questions_edit');
            Route::get('/questionnaires/questions/delete/{id}', 'Questionnaires\AdminQuestionnairesController@delete')->where('id', '[0-9]+')->name('questions_delete');

            Route::get('/questionnaires/questions/{question_id}/answers', 'Questionnaires\AdminAnswersController@index')->where('question_id', '[0-9]+')->name('answers_index');
            Route::get('/questionnaires/questions/{question_id}/answers/create', 'Questionnaires\AdminAnswersController@create')->where('question_id', '[0-9]+')->name('answers_create');
            Route::post('/questionnaires/questions/{question_id}/answers/create', 'Questionnaires\AdminAnswersController@store')->where('question_id', '[0-9]+')->name('answers_store');
            Route::get('/questionnaires/questions/{question_id}/answers/{answer_id}', 'Questionnaires\AdminAnswersController@show')->where('question_id', '[0-9]+')->where('answer_id', '[0-9]+')->name('answers_detail');
            Route::post('/questionnaires/questions/{question_id}/answers/{answer_id}', 'Questionnaires\AdminAnswersController@edit')->where('question_id', '[0-9]+')->where('answer_id', '[0-9]+')->name('answers_edit');
            Route::get('/questionnaires/questions/{question_id}/answers/{answer_id}/delete', 'Questionnaires\AdminAnswersController@delete')->where('question_id', '[0-9]+')->where('answer_id', '[0-9]+')->name('answers_delete');
        });
});
