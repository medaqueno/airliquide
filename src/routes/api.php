<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
/*
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

*/


    Route::middleware('throttle:4,1')->post('auth/checkPhone', 'Auth\CheckPhoneController');
    Route::middleware('throttle:4,1')->post('auth/checkSentCode', 'Auth\CheckSentCodeController');

    // Validate Vitalaire COde
    Route::post('auth/validateCode', 'Auth\ValidateCodeController');

    Route::middleware('throttle:4,1')->post('auth/register', 'Auth\RegisterController');
    Route::middleware('throttle:4,1')->post('auth/login', 'Auth\LoginController');

    // Only test purpose
    // Route::get('auth/odigo', 'Odigo\OdigoController');

    Route::middleware('throttle:3,1')->post('auth/forgetPassword', 'Auth\ForgetPasswordController');

    Route::middleware(['auth:sanctum'])->group(function () {
        Route::get('user', 'Users\GetUserDataController');
        Route::patch('user', 'Users\UpdateUserDataController');
        Route::put('user/changePassword', 'Users\ChangePasswordController');



        Route::post('suggestions', 'Suggestions\CreateSuggestionController');
        Route::get('news', 'News\GetActiveNewsController');
        Route::get('news/{id}', 'News\GetByIdNewsController')->where('id', '[0-9]+');

        Route::get('faqs', 'Faqs\GetAllFaqsController');
        Route::get('faqs/{id}', 'Faqs\GetByIdFaqsController')->where('id', '[0-9]+');

        Route::get('questionnaires/{questionnaires_id}/{therapies_id}/questions', 'Questionnaires\GetQuestionsController')->where('questionnaires_id', '[0-9]+')->where('therapies_id', '[0-9]+');

        Route::post('questionnaires/{questionnaires_id}/questions/answer ', 'Questionnaires\AnswerQuestionsController')->where('questionnaires_id', '[0-9]+');

        Route::get('therapies', 'Therapies\GetTherapiesByUserController');
        Route::post('therapies/{therapies_id}/register', 'Therapies\CreateTherapyRegistryController')->where('therapies_id', '[0-9]+');

        Route::get('therapies/{therapies_id}/history', 'Therapies\GetTherapiesHistoryController');

        Route::get('legals/all', 'Legals\GetAllLegalsController');
    });
