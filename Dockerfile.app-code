#############################################
# BUILD STAGE
#############################################

FROM vitalaire-base as builder

# Received from CLI
# Default Value PRE
ARG ENVIRONMENT=pre

# ---------------------------------------------
# INSTALL DEPENDENCIES
# ---------------------------------------------
RUN mkdir -p /var/www/html
WORKDIR /var/www/html

# COPY CODE
# 82 is www-data in Alpine
COPY --chown=82:82 ./src/ .
COPY --chown=82:82 ./src/.env.$ENVIRONMENT ./.env

#COPY --chown=82:82
#COPY ./src/composer.json .
#COPY ./src/composer.lock .

# ---------------------------------------------
# INSTALL COMPOSER
# Install dependencies, from composer.lock (as it is present too.)
# ---------------------------------------------
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer && \
    composer global require hirak/prestissimo --no-plugins --no-scripts && \
    composer install

#############################################
# CODE STAGE
# Only necessary stuff to run the code
#############################################

FROM vitalaire-base

# ---------------------------------------------
# ENV VARIABLES FOR OPCACHE
# PHP_OPCACHE_VALIDATE_TIMESTAMPS = 1 FOR DEV. 0 FOR PROD # MAY BE OVERRIDEN IN AWS ECS
# ---------------------------------------------
# if ENV VARIABLE PHP_OPCACHE_VALIDATE_TIMESTAMPS is null, set it to '1' (or leave as is otherwise).
ENV PHP_OPCACHE_VALIDATE_TIMESTAMPS="${PHP_OPCACHE_VALIDATE_TIMESTAMPS:-1}" \
    PHP_OPCACHE_MAX_ACCELERATED_FILES="16229" \
    PHP_OPCACHE_MEMORY_CONSUMPTION="192" \
    PHP_OPCACHE_INTERNED_STRINGS_BUFFER="16" \
    PHP_OPCACHE_MAX_WASTED_PERCENTAGE="10"

# ---------------------------------------------
# ADD CODE
# ---------------------------------------------
RUN mkdir -p /var/www/html
WORKDIR /var/www/html

# COPY APP CODE
# 82 is www-data in Alpine
COPY --chown=82:82 --from=builder /var/www/html .

COPY ./configs-docker/docker-entrypoint-prod.sh /tmp/docker-entrypoint.sh

# Give permissions to folders
RUN mkdir -p /var/www/html/storage/logs && \
    mkdir -p /var/www/html/storage/app && \
    mkdir -p /var/www/html/storage/framework/sessions && \
    mkdir -p /var/www/html/storage/framework/views && \
    mkdir -p /var/www/html/storage/framework/cache && \
#    chown -R 82:82 /var/www/html && \
    chmod -R 755 /var/www/html/storage && \
    chmod -R 755 /var/www/html/bootstrap/cache && \
    chown -R 82:82 /tmp && \
    chmod -R 755 /tmp

# ---------------------------------------------
# Expose the port PHP-FPM is reachable on
# ---------------------------------------------
EXPOSE 9000

ENTRYPOINT ["/tmp/docker-entrypoint.sh"]